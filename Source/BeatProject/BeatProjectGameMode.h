// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BeatProjectGameMode.generated.h"

UCLASS(minimalapi)
class ABeatProjectGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    ABeatProjectGameMode();
};