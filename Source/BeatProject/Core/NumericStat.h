// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "NumericStat.generated.h"

/**
 * 
 */
UCLASS(EditInlineNew)
class BEATPROJECT_API UNumericStat : public UObject
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NumericStat", meta = (DisplayName = "Current Value", AllowPrivateAccess = "true"))
        float CurrentValue;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NumericStat", meta = (DisplayName = "Constant Base Value", AllowPrivateAccess = "true"))
        float BaseValue;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NumericStat", meta = (DisplayName = "Current Max Value", AllowPrivateAccess = "true"))
        float CurrentMaxValue;

    //Absolute minimum value for this Stat
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NumericStat", meta = (DisplayName = "Min Value", AllowPrivateAccess = "true"))
        float MinValue;

    //Absolute maximum value for this Stat
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NumericStat", meta = (DisplayName = "Max Value", AllowPrivateAccess = "true"))
        float MaxValue;

public:

    UNumericStat();
    UNumericStat(float Value, float MinValue, float MaxValue);

    /** Check current percentage between current value and current max value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetCurrentPercentage"))
        float GetCurrentPercentage() const;

    /** Add or subtract a scalar number to the current value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "ModifyCurrentScalar"))
        void ModifyCurrentScalar(float Value);

    /** Add or subtract a percentage to the current value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "ModifyCurrentPercentage"))
        void ModifyCurrentPercentage(float Value);

    /**	Set a number to the current value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "SetCurrent"))
        void SetCurrent(float Value);

    /**	Get the current value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetCurrent"))
        float GetCurrent() const;

    /**	Get the base value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetBase"))
        float GetBase() const;

    /**	Set the base to a value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "SetBase"))
        void SetBase(float NewBase);

    /**	Get the current max value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetCurrentMax"))
        float GetCurrentMax() const;

    /**	Get the min value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetMin"))
        float GetMin() const;

    /**	Get the max value */
    UFUNCTION(BlueprintCallable, Category = "NumericStat", meta = (DisplayName = "GetMax"))
        float GetMax() const;
};
