// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BeatProject/BeatProject.h"
#include "UObject/NoExportTypes.h"
#include "Attack.generated.h"


class UBaseAction;
class ABeatManager;
class ABeatProjectCharacter;
class UBaseAttackAction;
/**
 *
 */
UCLASS(BlueprintType, Blueprintable)
class BEATPROJECT_API UAttack : public UObject
{
    GENERATED_BODY()

public:

    UAttack();

    /* Owner Character */
    UPROPERTY(BlueprintReadOnly, Category = "Beat|Attack", meta = (DisplayName = "Owner Character"))
        ABeatProjectCharacter* OwnerCharacter;

        /* BeatManager */
    UPROPERTY(BlueprintReadOnly, Category = "Beat|Attack", meta = (DisplayName = "Beat Manager"))
        ABeatManager* BeatManager;

    /* Action damage */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|Attack", meta = (DisplayName = "Damage"))
        float Damage;

    /* To say if damage is percentual or not */
    UPROPERTY(EditAnywhere, Category = "Beat|Attack", meta = (DisplayName = "Percentual Damage"))
        bool PercentualDamage;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Attack", meta = (DisplayName = "Anticipation Action"))
        UBaseAttackAction* AnticipationAction;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Attack", meta = (DisplayName = "Attack Action"))
        UBaseAttackAction* AttackAction;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Attack", meta = (DisplayName = "Recovery Action"))
        UBaseAttackAction* RecoveryAction;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Attack", meta = (DisplayName = "Current Action"))
        UBaseAttackAction* CurrentAction;

private:
    int CurrentActionBeatsLeft;


public:

    bool TryPlayAction();

    void Initialize(ABeatProjectCharacter& Owner, ABeatManager& NewBeatManager);

    UFUNCTION()
        void UpdateNextActionOnBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper, int32 TimeSignatureLower);

    UFUNCTION()
        void OnCancelCurrentAttack(UBaseAction* InstigatorAction);

    UFUNCTION()
        void FinishAttack();

    UFUNCTION()
    void RemoveDelegates();
};
