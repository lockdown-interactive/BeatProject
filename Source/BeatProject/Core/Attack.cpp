// Fill out your copyright notice in the Description page of Project Settings.


#include "Attack.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/CombatComponent.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Managers/BeatManager.h"
#include "FMODAudioComponent.h"

UAttack::UAttack()
    : Damage(15)
    , PercentualDamage(false)
{
}

bool UAttack::TryPlayAction()
{
    if (AnticipationAction->IsValidLowLevel())
    {
        CurrentAction = AnticipationAction;
    }
    else if (AttackAction->IsValidLowLevel())
    {
        CurrentAction = AttackAction;
    }

    if (CurrentAction->IsValidLowLevel())
    {
        const bool CanPlayAction = CurrentAction->TryPlayAction();

        if (CanPlayAction)
        {
            OwnerCharacter->StatsComponent->SetHyperArmor(true);
            OwnerCharacter->OnAttackStartDelegate.Broadcast();

            if(AnticipationAction || RecoveryAction)
            {
                BeatManager->OnBeatDelegate.AddDynamic(this, &UAttack::UpdateNextActionOnBeat);
            }
            
            CurrentActionBeatsLeft = FMath::FloorToInt(CurrentAction->Duration);
        }

        return CanPlayAction;
        //TODO: Check if actions in before beat are launched On beat or instantly
    }

    return false;
}

void UAttack::Initialize(ABeatProjectCharacter& Owner, ABeatManager& NewBeatManager)
{
    OwnerCharacter = &Owner;
    BeatManager = &NewBeatManager;

    if (AnticipationAction->IsValidLowLevel())
    {
        AnticipationAction->InitializeAction(Owner, NewBeatManager);
        AnticipationAction->OnActionStartDelegate.AddDynamic(Owner.CombatComponent, &UCombatComponent::ActivateResetComboTimer);
        AnticipationAction->OnActionInterruptDelegate.AddDynamic(this, &UAttack::OnCancelCurrentAttack);
    }

    if (AttackAction->IsValidLowLevel())
    {
        AttackAction->InitializeAction(Owner, NewBeatManager);
        if (!AnticipationAction)
        {
            AttackAction->OnActionStartDelegate.AddDynamic(Owner.CombatComponent, &UCombatComponent::ActivateResetComboTimer);
        }

        if(!RecoveryAction)
        {
            AttackAction->OnActionEndDelegate.AddDynamic(this, &UAttack::FinishAttack);
        }
        
        AttackAction->OnActionInterruptDelegate.AddDynamic(this, &UAttack::OnCancelCurrentAttack);
    }

    if (RecoveryAction->IsValidLowLevel())
    {
        RecoveryAction->InitializeAction(Owner, NewBeatManager);
        RecoveryAction->OnActionInterruptDelegate.AddDynamic(this, &UAttack::OnCancelCurrentAttack);
    }
        
}

void UAttack::UpdateNextActionOnBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper, int32 TimeSignatureLower)
{
    CurrentActionBeatsLeft -= 1;

    if (CurrentAction->IsValidLowLevel() && CurrentActionBeatsLeft == 0)
    {
        if (CurrentAction == AnticipationAction && AttackAction->IsValidLowLevel())
        {
            //Finishing anticipation
            CurrentAction = AttackAction;
            CurrentActionBeatsLeft = CurrentAction->Duration;
        }
        else if (CurrentAction == AttackAction && RecoveryAction->IsValidLowLevel())
        {
            //Finishing attack
            CurrentAction = RecoveryAction;
            CurrentActionBeatsLeft = CurrentAction->Duration;
            OwnerCharacter->StatsComponent->SetHyperArmor(false);
        }
        else
        {
            //Finishing recover
            FinishAttack();
        }

        //TODO: Check que magicamente coinciden
        if (CurrentAction->IsValidLowLevel())
        {
            CurrentAction->TryPlayAction();
        }

    }
}

void UAttack::OnCancelCurrentAttack(UBaseAction* InstigatorAction)
{
    if(!Cast<UBaseAttackAction>(InstigatorAction))
    {
        //When attack is cancelled by externally actions
        RemoveDelegates();
        OwnerCharacter->OnAttackInterruptedDelegate.Broadcast();
        CurrentAction = nullptr;
    }
}

void UAttack::FinishAttack()
{
    CurrentActionBeatsLeft = -1;
    OwnerCharacter->StatsComponent->SetHyperArmor(false);

    RemoveDelegates();

    //Reset Combo
    if (OwnerCharacter->CombatComponent->CurrentAttack)
    {
        OwnerCharacter->CombatComponent->UpdateCurrentAction();
    }
    else
    {
        //To reset in PlayAction()
        OwnerCharacter->CombatComponent->CurrentActionListIndex = 0;
    }

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::IdleRun);

    
    CurrentAction = nullptr;
    OwnerCharacter->OnAttackEndDelegate.Broadcast();
}

void UAttack::RemoveDelegates()
{
    //Removing delegates
    BeatManager->OnBeatDelegate.RemoveDynamic(this, &UAttack::UpdateNextActionOnBeat);
}
