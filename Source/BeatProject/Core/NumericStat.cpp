// Fill out your copyright notice in the Description page of Project Settings.


#include "NumericStat.h"

UNumericStat::UNumericStat()
    : CurrentValue(100)
    , BaseValue(100)
    , CurrentMaxValue(BaseValue)
    , MinValue(0)
    , MaxValue(1000)
{
}

UNumericStat::UNumericStat(float Value, float MinValue, float MaxValue)
    : CurrentValue(Value)
    , BaseValue(Value)
    , CurrentMaxValue(BaseValue)
    , MinValue(MinValue)
    , MaxValue(MaxValue)
{
}

float UNumericStat::GetCurrentPercentage() const
{
    return (CurrentValue / CurrentMaxValue * 100);
}

void UNumericStat::ModifyCurrentScalar(float Value)
{
    CurrentValue += Value;
    CurrentValue = FMath::Clamp<float>(CurrentValue, MinValue, CurrentMaxValue);
}

void UNumericStat::ModifyCurrentPercentage(float Value)
{
    CurrentValue += CurrentMaxValue * Value * 0.01;
    CurrentValue = FMath::Clamp<float>(CurrentValue, MinValue, CurrentMaxValue);
}

void UNumericStat::SetCurrent(float Value)
{
    CurrentValue = FMath::Clamp<float>(Value, MinValue, CurrentMaxValue);
}

float UNumericStat::GetCurrent() const
{
    return CurrentValue;
}

float UNumericStat::GetBase() const
{
    return BaseValue;
}

void UNumericStat::SetBase(float NewBase)
{
    if (NewBase >= MinValue && NewBase <= MaxValue)
    {
        BaseValue = NewBase;
        CurrentMaxValue = NewBase;
        CurrentValue = NewBase;
    }
}

float UNumericStat::GetCurrentMax() const
{
    return CurrentMaxValue;
}

float UNumericStat::GetMin() const
{
    return MinValue;
}

float UNumericStat::GetMax() const
{
    return MaxValue;
}
