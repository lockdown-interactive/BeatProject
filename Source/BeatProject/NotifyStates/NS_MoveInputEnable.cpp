// Fill out your copyright notice in the Description page of Project Settings.

#include "NS_MoveInputEnable.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/BeatProjectCharacter.h"


UNS_MoveInputEnable::UNS_MoveInputEnable()
{
}

void UNS_MoveInputEnable::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    if (MeshComp->GetOwner()->IsValidLowLevel())
    {
        OwnerCharacter = Cast<ABeatProjectCharacter>(MeshComp->GetOwner());
        if (OwnerCharacter->IsValidLowLevel() && OwnerCharacter->StatsComponent->IsValidLowLevel())
        {
            OwnerCharacter->StatsComponent->SetMoveInputEnable(false);
        }
    }
}

void UNS_MoveInputEnable::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{

}

void UNS_MoveInputEnable::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    if (OwnerCharacter->IsValidLowLevel() && OwnerCharacter->StatsComponent->IsValidLowLevel())
    {
        OwnerCharacter->StatsComponent->SetMoveInputEnable(true);
    }
}

