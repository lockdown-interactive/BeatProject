// Fill out your copyright notice in the Description page of Project Settings.


#include "NS_EndAnimation.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Actions/BeatAction.h"


UNS_EndAnimation::UNS_EndAnimation()
{
}

void UNS_EndAnimation::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    if (MeshComp->GetOwner()->IsValidLowLevel())
    {
        OwnerCharacter = Cast<ABeatProjectCharacter>(MeshComp->GetOwner());
        if(OwnerCharacter)
        {
            AnimationAction = OwnerCharacter->CurrentAction;
        }
    }
}

void UNS_EndAnimation::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
}

void UNS_EndAnimation::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    if (OwnerCharacter->IsValidLowLevel())
    {
        if (AnimationAction->IsValidLowLevel()) //Action Animation
        {
            if(AnimationAction->GetCurrentProgressState() != EProgressState::Cancel)
            {
                AnimationAction->ActionEnded();
            }
        }
    }
}