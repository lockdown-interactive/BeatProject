// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "BeatProject/BeatProject.h"
#include "NS_EndAnimation.generated.h"

class UBaseAction;
class ABeatProjectCharacter;

/* If you choose anticipation, the duration of Notify Reset Action State in montage need to be 1 or 2 frames less than the animation total duration */
UCLASS()
class BEATPROJECT_API UNS_EndAnimation : public UAnimNotifyState
{
    GENERATED_BODY()

public:

private:

    virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
    virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;
    virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

public:
    UNS_EndAnimation();

private:
    ABeatProjectCharacter* OwnerCharacter;
    UBaseAction* AnimationAction;
};
