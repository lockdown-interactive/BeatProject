// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "BeatProject/BeatProject.h"

#include "NS_AttackDisplacement.generated.h"

class UBaseAttackAction;
class ABeatProjectCharacter;

/**
 *
 */
UCLASS()
class BEATPROJECT_API UNS_AttackDisplacement : public UAnimNotifyState
{
    GENERATED_BODY()

public:

private:
    virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
    virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;
    virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

public:
    UNS_AttackDisplacement();

private:
    ABeatProjectCharacter* OwnerCharacter;
    UBaseAttackAction* AttackAction;
};
