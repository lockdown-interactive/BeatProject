// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "NS_ActionsInputEnable.generated.h"

class ABeatProjectCharacter;
/**
 *
 */
UCLASS()
class BEATPROJECT_API UNS_ActionsInputEnable : public UAnimNotifyState
{
    GENERATED_BODY()

private:
    virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
    virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;
    virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

public:
    UNS_ActionsInputEnable();

private:
    ABeatProjectCharacter* OwnerCharacter;
};
