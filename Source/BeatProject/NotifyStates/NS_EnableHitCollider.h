// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "NS_EnableHitCollider.generated.h"

class ABeatProjectCharacter;

//Hitbox Shape
UENUM(BlueprintType)
enum class EHitboxShape : uint8
{
    Box UMETA(DisplayName = "Box"),
    Sphere UMETA(DisplayName = "Sphere"),
    Capsule UMETA(DisplayName = "Capsule")
};
/**
 *
 */
UCLASS()
class BEATPROJECT_API UNS_EnableHitCollider : public UAnimNotifyState
{
    GENERATED_BODY()

        virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
    virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;
    virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

public:
    UNS_EnableHitCollider();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|Defense", meta = (DisplayName = "Blockable"))
        bool Blockable;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Shape", meta = (DisplayName = "Hitbox shape"))
        EHitboxShape CurrentHitboxShape;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Shape", meta = (DisplayName = "Hitbox thickness"))
        float CurrentHitboxLineThickness;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Width", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
        float BoxWidth;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Height", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
        float BoxHeight;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Depth", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
        float BoxDepth;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Sphere Shape", meta = (DisplayName = "Sphere Radius", EditCondition = "CurrentHitboxShape == EHitboxShape::Sphere"))
        float SphereRadius;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Capsule Shape", meta = (DisplayName = "Capsule Height", EditCondition = "CurrentHitboxShape == EHitboxShape::Capsule"))
        float CapsuleHeight;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox Capsule Shape", meta = (DisplayName = "Capsule Radius", EditCondition = "CurrentHitboxShape == EHitboxShape::Capsule"))
        float CapsuleRadius;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (DisplayName = "Socket Name"))
        FName SocketName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (DisplayName = "Print debug"))
        bool PrintDebug;

private:
    ABeatProjectCharacter* HitInstigatorCharacter;
    TArray<ABeatProjectCharacter*> AlreadyHitCharacters;
};
