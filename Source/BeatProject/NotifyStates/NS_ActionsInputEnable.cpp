// Fill out your copyright notice in the Description page of Project Settings.


#include "NS_ActionsInputEnable.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/BeatProjectCharacter.h"

UNS_ActionsInputEnable::UNS_ActionsInputEnable()
{
}

void UNS_ActionsInputEnable::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    if (MeshComp->GetOwner()->IsValidLowLevel())
    {
        OwnerCharacter = Cast<ABeatProjectCharacter>(MeshComp->GetOwner());
        if (OwnerCharacter->IsValidLowLevel() && OwnerCharacter->StatsComponent->IsValidLowLevel())
        {
            OwnerCharacter->StatsComponent->SetActionsInputEnable(false);
        }
    }
}

void UNS_ActionsInputEnable::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{

}

void UNS_ActionsInputEnable::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    if (OwnerCharacter->IsValidLowLevel() && OwnerCharacter->StatsComponent->IsValidLowLevel())
    {
        OwnerCharacter->StatsComponent->SetActionsInputEnable(true);
    }
}