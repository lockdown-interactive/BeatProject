// Fill out your copyright notice in the Description page of Project Settings.


#include "NS_AttackDisplacement.h"
#include "BeatProject/BeatProject.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "BeatProject/Components/CombatComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Core/Attack.h"

UNS_AttackDisplacement::UNS_AttackDisplacement()
{
}

void UNS_AttackDisplacement::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    if (MeshComp->GetOwner()->IsValidLowLevel())
    {
        OwnerCharacter = Cast<ABeatProjectCharacter>(MeshComp->GetOwner());

        if (OwnerCharacter->IsValidLowLevel() && OwnerCharacter->CombatComponent->CurrentAttack->IsValidLowLevel() && OwnerCharacter->CombatComponent->CurrentAttack->CurrentAction->IsValidLowLevel())
        {
            AttackAction = Cast<UBaseAttackAction>(OwnerCharacter->CombatComponent->CurrentAttack->CurrentAction);

            if (AttackAction->IsValidLowLevel())
            {
                float DisplacementLength = TotalDuration / AttackAction->GetCurrentPlayRate();

                //If the Aim Assist is active, rotate to the target just before start the displacement
                if (AttackAction->AimAssistActive)
                {
                    AttackAction->OwnerCharacter->SetActorRotation(AttackAction->AimAssistRotator);
                }

                AttackAction->SetUpDisplacement(AttackAction->RealDisplacementDistance);
                AttackAction->TimelineAttack.SetTimelineLength(DisplacementLength);
                AttackAction->TimelineAttack.PlayFromStart();
            }

        }
    }
}

void UNS_AttackDisplacement::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
    if (OwnerCharacter->IsValidLowLevel() && AttackAction->IsValidLowLevel())
    {

        //Advancing the timeline in every tick
        AttackAction->TimelineAttack.TickTimeline(FrameDeltaTime);


        FVector TargetPosition = FMath::Lerp(AttackAction->InitialPosition, AttackAction->FinalPosition, AttackAction->CurrentCurveValue);
        FVector VelToTarget = (TargetPosition - OwnerCharacter->GetActorLocation()) / FrameDeltaTime;

        OwnerCharacter->GetMovementComponent()->Velocity = VelToTarget;
    }
}

void UNS_AttackDisplacement::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    if (AttackAction->IsValidLowLevel())
    {
        AttackAction->FinishDisplacement();
    }
}