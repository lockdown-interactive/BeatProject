// Fill out your copyright notice in the Description page of Project Settings.


#include "NS_EnableHitCollider.h"

#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Components/CombatComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "DrawDebugHelpers.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Core/Attack.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "BeatProject/Actions/ParriedAction.h"
#include "BeatProject/Actions/ParryAction.h"
#include "BeatProject/Managers/BeatManager.h"

UNS_EnableHitCollider::UNS_EnableHitCollider()
    : CurrentHitboxShape(EHitboxShape::Box)
    , CurrentHitboxLineThickness(0.1f)
    , BoxWidth(0.f)
    , BoxHeight(0.f)
    , BoxDepth(0.f)
    , SphereRadius(0.f)
    , CapsuleHeight(0.f)
    , CapsuleRadius(0.f)
    , SocketName("")
    , PrintDebug(false)
{

}

void UNS_EnableHitCollider::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    HitInstigatorCharacter = Cast<ABeatProjectCharacter>(MeshComp->GetOwner());
    AlreadyHitCharacters.Empty();
}

void UNS_EnableHitCollider::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
    FCollisionShape CollisionShape;
    TArray<FHitResult> OutHits;
    const FVector SocketLocation = MeshComp->GetSocketTransform(SocketName).GetLocation();
    const FQuat SocketRotation = MeshComp->GetSocketTransform(SocketName).GetRotation();

    if (CurrentHitboxShape == EHitboxShape::Sphere)
    {
        CollisionShape = FCollisionShape::MakeSphere(SphereRadius);

        if (PrintDebug)
        {
            DrawDebugSphere(MeshComp->GetWorld(), SocketLocation, SphereRadius, 32, FColor::Green, false, -1, 0, CurrentHitboxLineThickness);
        }

    }
    else if (CurrentHitboxShape == EHitboxShape::Box)
    {
        FVector BoxDimensions(BoxWidth * 0.5f, BoxHeight * 0.5f, BoxDepth * 0.5f);
        CollisionShape = FCollisionShape::MakeBox(BoxDimensions);

        if (PrintDebug)
        {
            DrawDebugBox(MeshComp->GetWorld(), SocketLocation, BoxDimensions, SocketRotation, FColor::Blue, false, -1, 0, CurrentHitboxLineThickness);
        }

    }
    else if (CurrentHitboxShape == EHitboxShape::Capsule)
    {
        CollisionShape = FCollisionShape::MakeCapsule(CapsuleRadius, CapsuleHeight * 0.5f);

        if (PrintDebug)
        {
            DrawDebugCapsule(MeshComp->GetWorld(), SocketLocation, CapsuleHeight * 0.5f, CapsuleRadius, SocketRotation, FColor::Green, false, -1, 0, CurrentHitboxLineThickness);
        }

    }

    if (HitInstigatorCharacter->IsValidLowLevel())
    {
        FCollisionQueryParams TraceParams(FName(TEXT("InteractTrace")), true, NULL);
        //TraceParams.bTraceComplex = true;
        TraceParams.AddIgnoredActor(HitInstigatorCharacter); //Ignore himself
        bool Hit = MeshComp->GetWorld()->SweepMultiByChannel(
            OutHits,
            SocketLocation,
            SocketLocation,
            SocketRotation,
            ECC_Pawn,
            CollisionShape,
            TraceParams
        );

        //Check collided Pawns
        if (Hit)
        {
            //loop through Tarray
            for (auto& HitCharacter : OutHits)
            {
                //ABeatProjectCharacter who received the hit
                ABeatProjectCharacter* HitReceivedCharacter = Cast<ABeatProjectCharacter>(HitCharacter.GetActor());

                if (HitReceivedCharacter->IsValidLowLevel() && !AlreadyHitCharacters.Contains(HitReceivedCharacter) && HitReceivedCharacter->StatsComponent->GetCurrentExclusiveState() != EMutuallyExclusiveState::Dead && !HitReceivedCharacter->StatsComponent->NonExclusiveStates.Invulnerable)
                {
                    UAttack* AttackAction = Cast<UAttack>(HitInstigatorCharacter->CombatComponent->CurrentAttack);

                    AAIController* HitInstigatorEnemy = Cast<AAIController>(HitInstigatorCharacter->GetController());
                    AAIController* HitReceivedEnemy = Cast<AAIController>(HitReceivedCharacter->GetController());

                    //Check one of the characters is not an enemy
                    if (!HitInstigatorEnemy->IsValidLowLevel() || !HitReceivedEnemy->IsValidLowLevel())
                    {
                        if (AttackAction->IsValidLowLevel())
                        {
                            //Parry
                            if (HitReceivedCharacter->StatsComponent->GetCurrentExclusiveState() == EMutuallyExclusiveState::Block && Blockable)
                            {
                                float BeatDuration = HitInstigatorCharacter->BeatManager->GetSecondsBetweenBeats();

                                //Parry Received
                                bool ParryDone = HitInstigatorCharacter->ParriedAction->TryPlayAction();

                                //Parry Done
                                if (ParryDone)
                                {
                                    HitReceivedCharacter->ParryAction->TryPlayAction();
                                }
                            }
                            else //Attack
                            {
                                float Damage = AttackAction->Damage;
                                HitInstigatorCharacter->EOnHitDelegate.Broadcast(HitReceivedCharacter, AttackAction->CurrentAction->Type, Damage);
                                HitReceivedCharacter->EOnHitReceivedDelegate.Broadcast(HitInstigatorCharacter, AttackAction->CurrentAction->Type, Damage, AttackAction->PercentualDamage);
                            }
                        }
                    }
                    AlreadyHitCharacters.Add(HitReceivedCharacter);
                }
            }
        }
    }
}

void UNS_EnableHitCollider::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
}

