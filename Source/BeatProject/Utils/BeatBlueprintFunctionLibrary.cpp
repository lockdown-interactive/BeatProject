// Fill out your copyright notice in the Description page of Project Settings.


#include "BeatBlueprintFunctionLibrary.h"

bool UBeatBlueprintFunctionLibrary::IsWithEditor()
{
#if WITH_EDITOR
     return true;
#else
     return false;
#endif
}
