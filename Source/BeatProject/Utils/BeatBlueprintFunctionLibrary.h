// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BeatBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UBeatBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	//! Allow the blueprint to determine whether we are running with the editor or not
 UFUNCTION( BlueprintPure, Category = Utils )
	static bool IsWithEditor();
};
