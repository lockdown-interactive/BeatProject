// Copyright Epic Games, Inc. All Rights Reserved.

#include "BeatProjectGameMode.h"
#include "BeatProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABeatProjectGameMode::ABeatProjectGameMode()
{
    // set default pawn class to our Blueprinted character
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
    if (PlayerPawnBPClass.Class != NULL)
    {
        DefaultPawnClass = PlayerPawnBPClass.Class;
    }
}
