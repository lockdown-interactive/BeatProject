// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define MINUTES_TO_MILLISECONDS 60000
#define MINUTES_TO_SECONDS 60.f

#define ECC_DASH_OBJECT ECC_GameTraceChannel1
#define ECC_DASH_TRACE_CHANNEL ECC_GameTraceChannel2

#define STRINGIFY(Identifier) #Identifier

#define EnumValueToString(EnumType, EnumValue) \
	FindObject<UEnum>(ANY_PACKAGE, TEXT(STRINGIFY(EnumType)), true)->GetNameStringByIndex(static_cast<uint8>(EnumValue));

UENUM(BlueprintType)
enum class EBeatState : uint8
{
    Beat        UMETA(DisplayName = "Beat"),
    BeforeBeat  UMETA(DisplayName = "Before Beat"),
    AfterBeat   UMETA(DisplayName = "After Beat"),
    OutBeat     UMETA(DisplayName = "Out of Beat")
};

UENUM(BlueprintType)
enum class EAttackType : uint8
{
    None    UMETA(DisplayName = "None"),
    Block   UMETA(DisplayName = "Block"),
    Normal  UMETA(DisplayName = "Normal"),
    Heavy   UMETA(DisplayName = "Heavy"),
    Sweep   UMETA(DisplayName = "Sweep")
};

UENUM(BlueprintType) //"BlueprintType" is essential to include
enum class ENumericStat : uint8
{
    Life			UMETA(DisplayName = "Life"),
    DamageTakenMult		UMETA(DisplayName = "DamageTakenMult"),
    BlockDamageTakenMult	UMETA(DisplayName = "BlockDamageTakenMult"),
    AudienceGauge	UMETA(DisplayName = "AudienceGauge"),
};

/**
 * Character actions which are mutually exclusive,
 * only one can be true and the other has to be changed to false
 */
 //TODO: Create Actions for every state
UENUM(BlueprintType)
enum class EMutuallyExclusiveState : uint8
{
    IdleRun	UMETA(DisplayName = "Idle Run"),
    Dash 	UMETA(DisplayName = "Dash"),
    Attack	UMETA(DisplayName = "Attack"),
    Block	UMETA(DisplayName = "Block"),
    Parry       UMETA(DisplayName = "Parry"),
    Parried     UMETA(DisplayName = "Parried"),
    Stun        UMETA(DisplayName = "Stun"),
    Hit         UMETA(DisplayName = "Hit"),
    Dead        UMETA(DisplayName = "Dead")
};

UENUM(BlueprintType) //"BlueprintType" is essential to include
enum class EBeatAccuracy : uint8
{
    Fail			UMETA(DisplayName = "Fail"),
    Poor			UMETA(DisplayName = "Poor"),
    Nice			UMETA(DisplayName = "Nice"),
    Perfect			UMETA(DisplayName = "Perfect")
};
