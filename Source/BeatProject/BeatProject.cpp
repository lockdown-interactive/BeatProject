// Copyright Epic Games, Inc. All Rights Reserved.

#include "BeatProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BeatProject, "BeatProject" );
 