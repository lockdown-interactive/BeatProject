// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAttackAction.h"

#include "DrawDebugHelpers.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Components/CombatComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Managers/BeatManager.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

UBaseAttackAction::UBaseAttackAction()
    : UseAimAssist(false)
    , OffsetDistanceToTarget(20.f)
    , RealDisplacementDistance(100.f)
    , AimAssistRotator(FRotator(0.f, 0.f, 0.f))
    , MaxRadiusAimAssist(1000)
    , MaxAngleAimAssist(25)
    , AimAssistDrawDebug(false)
    , AimAssistActive(false)
    , LastInputOnAttack(FVector::ZeroVector)
    , YawAngleToTarget(0.f)
    , AttackDisplacementDistance(100.f)
{
}

void UBaseAttackAction::InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager)
{
    Super::InitializeAction(BeatCharacter, WorldBeatManager);

    FOnTimelineFloat AttackProgressFunction;
    
    AttackProgressFunction.BindUFunction(this, FName("HandleAttackDisplacementProgress"));

    if (AttackDisplacementFloatCurve->IsValidLowLevel())
    {
        //Setting up the loop status and the function that is going to fire when the timeline ticks
        TimelineAttack.AddInterpFloat(AttackDisplacementFloatCurve, AttackProgressFunction);
    }
}

void UBaseAttackAction::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void UBaseAttackAction::PlayAction()
{
    Super::PlayAction();

    if (Type == EAttackType::Block)
    {
        OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::Block);
    }
    else
    {
        OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::Attack);
    }

    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.RemoveDynamic(this, &UBaseAttackAction::OnMovementCapsuleBeginOverlap);
    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &UBaseAttackAction::OnMovementCapsuleBeginOverlap);

    PreTick();
}

void UBaseAttackAction::PreStart()
{
    
}

void UBaseAttackAction::StartUpdate(float DeltaTime)
{
    //Anticipation Animations is executing here
}

void UBaseAttackAction::PostStart()
{
    CurrentProgressState = EProgressState::PreTick;
}

void UBaseAttackAction::PreTick()
{
    if(UseAimAssist)
    {
        SetUpAimAssist();
    } 

    bTickEnabled = true;

    PlayAnimation();

    TimelineAttack.SetTimelineLengthMode(ETimelineLengthMode::TL_TimelineLength);

    OnActionStartDelegate.Broadcast();
    
    CurrentProgressState = EProgressState::TickUpdate;

}

void UBaseAttackAction::TickUpdate(float DeltaTime)
{
    //Attack animation is playing 
}

void UBaseAttackAction::PostTick()
{
}

void UBaseAttackAction::PreEnd()
{
}

void UBaseAttackAction::EndUpdate(float DeltaTime)
{
}

void UBaseAttackAction::PostEnd()
{
    Super::PostEnd();

    if (Type == EAttackType::Block)
    {
        OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::IdleRun);
    }
}

void UBaseAttackAction::Cancel(UBaseAction* InstigatorAction)
{
    FinishDisplacement();
    Super::Cancel(InstigatorAction);
}

void UBaseAttackAction::SetUpDisplacement(float DisplacementDistance)
{
    //Deactivate Friction on Displacement
    OwnerCharacter->GetCharacterMovement()->BrakingFrictionFactor = 0.f;
    OwnerCharacter->GetCharacterMovement()->GravityScale = 0.f;

    //Setup Displacement Capsule
    OwnerCharacter->GetMovementCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    OwnerCharacter->GetMovementCapsuleComponent()->SetUseCCD(true);

    //Calculate positions
    InitialPosition = OwnerCharacter->GetActorLocation();
    FinalPosition = InitialPosition + OwnerCharacter->GetActorForwardVector() * DisplacementDistance;
    /*  UE_LOG(LogTemp, Warning, TEXT("TargetPosition: , %s"), *(FinalPosition.ToString()));*/
    CurrentCurveValue = 0;
}

void UBaseAttackAction::RestoreDisplacement()
{
    OwnerCharacter->GetCharacterMovement()->BrakingFrictionFactor = OwnerCharacter->DefaultFrictionFactor;
    OwnerCharacter->GetCharacterMovement()->GravityScale = OwnerCharacter->DefaultGravityScale;

    OwnerCharacter->GetMovementCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    OwnerCharacter->GetMovementCapsuleComponent()->SetUseCCD(false);

    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.RemoveDynamic(this, &UBaseAttackAction::OnMovementCapsuleBeginOverlap);
}

void UBaseAttackAction::HandleAttackDisplacementProgress(float Value)
{
    CurrentCurveValue = AttackDisplacementFloatCurve->GetFloatValue(TimelineAttack.GetPlaybackPosition() / TimelineAttack.GetTimelineLength());
}

void UBaseAttackAction::FinishDisplacement()
{
    OwnerCharacter->GetMovementComponent()->Velocity = FVector::ZeroVector;
    //UE_LOG(LogTemp, Warning, TEXT("EndPosition: , %s"), *(OwnerCharacter->GetActorLocation().ToString()));
    RestoreDisplacement();
}

float UBaseAttackAction::GetCurrentPlayRate()
{
    return PlayRate;
}

void UBaseAttackAction::ActionEnded()
{
    Super::ActionEnded();

    PostEnd();
}

bool UBaseAttackAction::CheckPreconditions()
{
    return Super::CheckPreconditions()  && OwnerCharacter->StatsComponent->GetActionsInputEnable();
}

void UBaseAttackAction::OnMovementCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                                      UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    FinishDisplacement();
}



void UBaseAttackAction::SetUpAimAssist()
{
    AimAssistTargetSearch();
    CalculateDisplacementDistanceToTarget();
}

void UBaseAttackAction::AimAssistTargetSearch()
{
    if (OwnerCharacter->IsValidLowLevel())
    {
        APlayerController* controller = Cast<APlayerController>(OwnerCharacter->Controller);
        if (controller->IsValidLowLevel())
        {
            LastInputOnAttack = OwnerCharacter->GetPlayerLastInput();
        }       

        //Input Management
        if (LastInputOnAttack.IsNearlyZero()) //There is not Input
        {
            if (TargetCharacter->IsValidLowLevel())
            {
                FVector ActorLocation = OwnerCharacter->GetActorLocation();
                FRotator RotatorToTarget = UKismetMathLibrary::FindLookAtRotation(ActorLocation, TargetCharacter->GetActorLocation());
                YawAngleToTarget = UKismetMathLibrary::NormalizedDeltaRotator(RotatorToTarget, OwnerCharacter->GetActorRotation()).Yaw;

                if (OwnerCharacter->GetDistanceTo(TargetCharacter) > MaxRadiusAimAssist //Target is further than limit distance
                        /*|| TargetCharacter->StatsComponent->GetCurrentExclusiveState() == EMutuallyExclusiveState::Dead*/
                    || abs(YawAngleToTarget) > MaxAngleAimAssist) //Target is outside the angle
                {
                    //Calculate new target
                    TargetCharacter = nullptr;
                }
            }
            //else if (!controller->IsValidLowLevel()) //The attacker is an AI. Maybe te AI Aim Assist could be here if in the future it needs to be more optimal
            //{
            //    TargetCharacter = Cast<ABeatProjectCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
            //    Resto de movidas del AimAssistTargetOnSphere(), solamente la parte de la AI
            //}
        }
        else //There is input
        {
            //Calculate new target
            TargetCharacter = nullptr;
        }       

        //If we did not maintain the current target, we search a new one.
        if (TargetCharacter == nullptr)
        {
            //Calculate the new target with the valid distance and angle
            AimAssistTargetOnSphere();
        }
    }
}

void UBaseAttackAction::CalculateDisplacementDistanceToTarget()
{
    if (TargetCharacter->IsValidLowLevel())
    {
        AimAssistActive = true;
        float DistanceToTarget = OwnerCharacter->GetDistanceTo(TargetCharacter);
        float OffsetDistance = OffsetDistanceToTarget;

        if (DistanceToTarget < OffsetDistance) //Attacker is inside the AttackAction's offset
        {
            RealDisplacementDistance = DistanceToTarget - OffsetDistance;
        }
        else //Desired displacement is less or greater than the attack displacement
        {
            RealDisplacementDistance = DistanceToTarget - OffsetDistance;
        }

        //Save the rotation to the target
        float FinalYaw = OwnerCharacter->GetActorRotation().Yaw + YawAngleToTarget;
        AimAssistRotator = FRotator(OwnerCharacter->GetActorRotation().Pitch, FinalYaw, OwnerCharacter->GetActorRotation().Roll);
    }
    else //No target, forward displacement
    {
        AimAssistActive = false;
        RealDisplacementDistance = AttackDisplacementDistance;
        AimAssistRotator = FRotator(0.f, 0.f, 0.f);
    }
}

void UBaseAttackAction::UpdateTargetAndAngle(FVector ActorLocation, ABeatProjectCharacter* TargetAimAssist)
{
    TargetCharacter = TargetAimAssist;

    FRotator RotatorToTarget = UKismetMathLibrary::FindLookAtRotation(ActorLocation, TargetCharacter->GetActorLocation());
    YawAngleToTarget = UKismetMathLibrary::NormalizedDeltaRotator(RotatorToTarget, OwnerCharacter->GetActorRotation()).Yaw;
}

void UBaseAttackAction::AimAssistTargetOnSphere()
{
    APlayerController* instigatorController = Cast<APlayerController>(OwnerCharacter->Controller);
    FVector ActorLocation = OwnerCharacter->GetActorLocation();
    FRotator ActorRotator;

    //Handle the collision actors
    TArray<FHitResult> OutHits;
    FCollisionShape MySphere = FCollisionShape::MakeSphere(MaxRadiusAimAssist);

    if (LastInputOnAttack.IsNearlyZero() || !instigatorController->IsValidLowLevel())
    {
        ActorRotator = OwnerCharacter->GetActorRotation();
    }
    else
    {
        //Convertion of the input direction vector to Rotator
        ActorRotator = LastInputOnAttack.Rotation();
    }

    if (AimAssistDrawDebug)
    {
        DrawDebugLine(GetWorld(), ActorLocation, ActorLocation + LastInputOnAttack * 250, FColor::Green, false, 5.f, 0, 10.f);
        DrawDebugSphere(GetWorld(), ActorLocation, MySphere.GetSphereRadius(), 32, FColor::Cyan, false, 5.f);
    }


    //Sphere collision
    float SphereHalfRadius = MySphere.GetSphereRadius() * 0.5f;
    float HalfCapsuleHeight = OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
    float ZOffsetSphere = SphereHalfRadius - HalfCapsuleHeight;

    ActorLocation = FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z - ZOffsetSphere);

    bool isHit = GetWorld()->SweepMultiByChannel(OutHits, ActorLocation, ActorLocation, FQuat::Identity, ECC_Pawn, MySphere);

    if (isHit)
    {
        for (auto& Hit : OutHits)
        {
            ABeatProjectCharacter* TargetAimAssist = Cast<ABeatProjectCharacter>(Hit.Actor);

            if (TargetAimAssist->IsValidLowLevel() && TargetAimAssist != OwnerCharacter /*&& TargetCharacter->StatsComponent->GetCurrentExclusiveState() != EMutuallyExclusiveState::Dead*/)
            {
                //Target Variables
                FVector TargetLocation = TargetAimAssist->GetActorLocation();
                FRotator RotationToTarget = UKismetMathLibrary::FindLookAtRotation(ActorLocation, TargetLocation);
                float yawAngle = UKismetMathLibrary::NormalizedDeltaRotator(RotationToTarget, ActorRotator).Yaw;

                if (abs(yawAngle) < MaxAngleAimAssist)
                {
                    if (TargetCharacter == nullptr)
                    {
                        //The Instigator is an AI
                        if(!instigatorController->IsValidLowLevel() && TargetAimAssist == GetWorld()->GetFirstPlayerController()->GetCharacter())
                        {
                            UpdateTargetAndAngle(ActorLocation, TargetAimAssist);
                            break;
                        }
                        else if(instigatorController->IsValidLowLevel())
                        {
                            UpdateTargetAndAngle(ActorLocation, TargetAimAssist);
                        }
                        
                    }
                    //Current enemy is closer than stored enemy
                    else if (OwnerCharacter->GetDistanceTo(TargetAimAssist) < OwnerCharacter->GetDistanceTo(TargetCharacter))
                    {
                        UpdateTargetAndAngle(ActorLocation, TargetAimAssist);
                    }
                }
            }
        }
    }
}
