// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BeatAction.h"
#include "Components/TimelineComponent.h"
#include "BeatProject/BeatProject.h"
#include "BaseAttackAction.generated.h"

/**
 *
 */
UCLASS(EditInlineNew)
class BEATPROJECT_API UBaseAttackAction : public UBeatAction
{
    GENERATED_BODY()

public:
    UBaseAttackAction();


    ////////////////////////////////////////
    ///             AIM ASSIST           ///
    ////////////////////////////////////////

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Use Aim Assist"))
        bool UseAimAssist;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Offset Distance To Target", EditCondition = "UseAimAssist"))
        float OffsetDistanceToTarget;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Real Displacement Distance", EditCondition = "UseAimAssist"))
        float RealDisplacementDistance;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Real Displacement Distance", EditCondition = "UseAimAssist"))
        FRotator AimAssistRotator;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Max Distance To Aim Assist", EditCondition = "UseAimAssist"))
        float MaxRadiusAimAssist;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Max Angle To Aim Assist", EditCondition = "UseAimAssist"))
        float MaxAngleAimAssist;

    UPROPERTY(EditAnywhere, Category = "Beat|AimAssist", meta = (DisplayName = "Aim Assist Draw Debug", EditCondition = "UseAimAssist"))
        bool AimAssistDrawDebug;

    bool AimAssistActive;
    FVector LastInputOnAttack;
    ABeatProjectCharacter* TargetCharacter;
    float YawAngleToTarget;

    ////////////////////////////////////////
    ///             ATTACK               ///
    ////////////////////////////////////////

    /* ATTACK MUST HAVE A CURVE */
    UPROPERTY(EditAnywhere, Category = "Beat|AttackAction", meta = (DisplayName = "Attack Displacement Curve"))
        UCurveFloat* AttackDisplacementFloatCurve;

    UPROPERTY()
        FTimeline TimelineAttack;

    UPROPERTY(EditAnywhere, Category = "Beat|AttackAction", meta = (DisplayName = "Attack Displacement Distance"))
        float AttackDisplacementDistance;

    /* Attack Type */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|AttackAction", meta = (DisplayName = "Type"))
        EAttackType Type;

    FVector InitialPosition;
    FVector FinalPosition;

    float CurrentCurveValue;

private:

    UPROPERTY()
        float PlayRate;

public:
    virtual void InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager) override;
    virtual void Tick(float DeltaTime) override;
    virtual void Cancel(UBaseAction* InstigatorAction) override;

    UFUNCTION()
        void SetUpDisplacement(float DisplacementDistance);

    void FinishDisplacement();

    float GetCurrentPlayRate();

    virtual void ActionEnded();

protected:
    bool CheckPreconditions() override;

    virtual void PlayAction() override;
    virtual void PreStart() override;
    virtual void StartUpdate(float DeltaTime) override;
    virtual void PostStart() override;
    virtual void PreTick() override;
    virtual void TickUpdate(float DeltaTime) override;
    virtual void PostTick() override;
    virtual void PreEnd() override;
    virtual void EndUpdate(float DeltaTime) override;
    virtual void PostEnd() override;

private:

    UFUNCTION()
        void HandleAttackDisplacementProgress(float Value);

    void RestoreDisplacement();

    UFUNCTION()
        void OnMovementCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
            UPrimitiveComponent* OtherComp,
            int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    ////////////////////////////////////////
    ///             AIM ASSIST           ///
    ////////////////////////////////////////
    
    void SetUpAimAssist();
    void AimAssistTargetSearch();
    void CalculateDisplacementDistanceToTarget();
    void UpdateTargetAndAngle(FVector ActorLocation, ABeatProjectCharacter* TargetAimAssist);
    void AimAssistTargetOnSphere();
};
