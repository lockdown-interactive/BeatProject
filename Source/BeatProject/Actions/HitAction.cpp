// Fill out your copyright notice in the Description page of Project Settings.


#include "HitAction.h"

#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"

UHitAction::UHitAction()
{
    MutuallyExclusiveState = EMutuallyExclusiveState::Hit;
}

bool UHitAction::CheckPreconditions()
{
    return Super::CheckPreconditions()
    && OwnerCharacter->StatsComponent->GetCurrentExclusiveState() != EMutuallyExclusiveState::Hit
    && !OwnerCharacter->StatsComponent->GetHyperArmor();
}
