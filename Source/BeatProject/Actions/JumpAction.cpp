// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpAction.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Managers/BeatManager.h"
#include "GameFramework/CharacterMovementComponent.h"


UJumpAction::UJumpAction()
    : JumpZDistance(250.f)
{
}

void UJumpAction::InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager)
{
    Super::InitializeAction(BeatCharacter, WorldBeatManager);
}

void UJumpAction::PlayAction()
{
    Super::PlayAction();

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::IdleRun);

    //It is an instant action, avoid use the pre, update, post structures
    SetUpJump();
    OwnerCharacter->Jump();

    ActionState = EActionState::Available;
}

bool UJumpAction::CheckPreconditions()
{
     return Super::CheckPreconditions() && OwnerCharacter->StatsComponent->GetActionsInputEnable();
}

void UJumpAction::SetUpJump()
{
    // https://youtu.be/hG9SzQxaCm8?t=779
    // T = input variable with jump duration in beats * bpm;
    // H = input variable with Maximum Height
    // [1]: v0 = 2H / (T / 2); Calculate JumpZVelocity
    // [2]: G = -2H / T^2; Calculate Character Gravity

    
    float JumpTotalTime = Duration * BeatManager->GetSecondsBetweenBeats() / 2;
    
    float CharacterGravity = -2 * JumpZDistance / pow(JumpTotalTime, 2);
    float CharacterGravityScale = CharacterGravity / GetWorld()->GetGravityZ();

    float InitialZVelocity = 2 * JumpZDistance / (JumpTotalTime);

    OwnerCharacter->GetCharacterMovement()->JumpZVelocity = InitialZVelocity;
    OwnerCharacter->GetCharacterMovement()->GravityScale = CharacterGravityScale;
}