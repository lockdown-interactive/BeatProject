// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseStateAction.h"

#include "DeadAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UDeadAction : public UBaseStateAction
{
	GENERATED_BODY()

public:
    UDeadAction();

protected:
    virtual bool CheckPreconditions() override;

    virtual void PostEnd() override;

};
