// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BaseAction.h"
#include "BeatProject/BeatProject.h"
#include "UObject/NoExportTypes.h"
#include "BeatAction.generated.h"

/***********************************************************************/
UCLASS()
class BEATPROJECT_API UBeatAction : public UBaseAction
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UBeatAction();

    // Action Value to increase the Audience Gauge on Nice, Perfect Accuracy
    UPROPERTY(EditDefaultsOnly, Category = "Beat|BeatAction", meta = (DisplayName = "Action Value", ClampMin = "0"))
        float ActionValue;

    // Action Value to decrease the Audience Gauge on Poor, Fail Accuracy
    UPROPERTY(EditDefaultsOnly, Category = "Beat|BeatAction", meta = (DisplayName = "Low Accuracy Action Value", ClampMin = "0"))
        float LowAccuracyActionValue;

    // Penalty Cooldown in beats 
    UPROPERTY(EditAnywhere, Category = "Beat|BeatAction", meta = (DisplayName = "Penalty Cooldown", ClampMin = "0"))
        float PenaltyCooldown;

    /* BeatManager */
    UPROPERTY(BlueprintReadOnly, Category = "Beat|BeatAction", meta = (DisplayName = "Beat Manager"))
        ABeatManager* BeatManager;

protected:
    /* Handle to manage the timer */
    FTimerHandle PenaltyCooldownTimerHandle;

    /* Offset before reach a beat (Milliseconds)*/
    float BeforeOffsetMillisecs;

    /* Offset after reach a beat (Milliseconds)*/
    float AfterOffsetMillisecs;

    /* Offset before reach a beat (Milliseconds)*/
    float BeforeOffsetBeats;

    /* Offset after reach a beat (Beat Percent)*/
    float AfterOffsetBeats;

public:
    virtual void InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager) override;

    virtual bool TryPlayAction() override;

    UFUNCTION()
        void UpdateBPM(int BPM);

    UFUNCTION()
        void OnTimelineBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper,
            int32 TimeSignatureLower);

    // Get Score Multiplier for the Action with a given Accuracy
    float GetScoreMultiplierFromDataTable(FName Accuracy);

protected:

    void ResetPenaltyCooldown();

    EBeatAccuracy GetActionAccuracy( float CurrentTimelinePosition);
    int GetComboSumFromDataTable(FName CurrentAccuracy);
    FFloatRange GetRangeFromDataTable(FName Accuracy);
};
