// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseStateAction.h"
#include "HitAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UHitAction : public UBaseStateAction
{
	GENERATED_BODY()

public:
    UHitAction();

protected:
    virtual bool CheckPreconditions() override;
};
