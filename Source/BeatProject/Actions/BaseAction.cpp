// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAction.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatAction.h"
#include "BeatProject/Managers/BeatManager.h"

UBaseAction::UBaseAction()
    : UseDuration(true)
    , Duration(1)
    , OwnerCharacter()
    , bTickEnabled(false)
    , CurrentProgressState(EProgressState::PreStart)
    , ActionState(EActionState::Available)
{
}

void UBaseAction::InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager)
{
    OwnerCharacter = &BeatCharacter;
}

bool UBaseAction::TryPlayAction()
{
    if (CheckPreconditions())
    {
        if (OwnerCharacter->CurrentAction->IsValidLowLevel())
        {
            OwnerCharacter->CurrentAction->Cancel(this);
        }

        PlayAction();

        return true;
    }
    return false;
}

void UBaseAction::Tick(float DeltaTime)
{
    //Don't invoke Super::Tick(), otherwise would link failed!!!
    //Super::Tick(DeltaTime);

    switch (CurrentProgressState)
    {
    case EProgressState::PreStart:
    {
        PreStart();
    }
    break;
    case EProgressState::StartUpdate:
    {
        StartUpdate(DeltaTime);
    }
    break;
    case EProgressState::PostStart:
    {
        PostStart();
    }
    break;
    case EProgressState::PreTick:
    {
        PreTick();
    }
    break;
    case EProgressState::TickUpdate:
    {
        TickUpdate(DeltaTime);
    }
    break;
    case EProgressState::PostTick:
    {
        PostTick();
    }
    break;
    case EProgressState::PreEnd:
    {
        PreEnd();
    }
    break;
    case EProgressState::EndUpdate:
    {
        EndUpdate(DeltaTime);
    }
    break;
    case EProgressState::PostEnd:
    {
        PostEnd();
    }
    break;
    case EProgressState::Cancel:
    {
        Cancel(this);
    }
    break;
    default:
        break;
    }
}

bool UBaseAction::IsTickable() const
{
    //notify engine to ignore Tick of the object constructed before game running.
    // Tick only if we are both NOT a template and if we are specifically not in-editor-before-beginplay is called.
    //return (!IsTemplate(RF_ClassDefaultObject)) && !(GIsEditor && !GWorld->HasBegunPlay()
   // GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "IsTickable: " + bIsCreateOnRunning ? "True" : "F");

    return bTickEnabled;
    //return bIsCreateOnRunning && bTickEnabled;
}

TStatId UBaseAction::GetStatId() const
{
    return UObject::GetStatID();
}

EProgressState UBaseAction::GetCurrentProgressState()
{
    return CurrentProgressState;
}


EActionState UBaseAction::GetActionState()
{
    return ActionState;
}

void UBaseAction::Cancel(UBaseAction* InstigatorAction)
{
    CurrentProgressState = EProgressState::Cancel;

    UpdateActionStateOnEnd();

    bTickEnabled = false;
    OnActionInterruptDelegate.Broadcast(InstigatorAction);
}

void UBaseAction::UpdateActionStateOnEnd()
{
    ActionState = EActionState::Available;
}

void UBaseAction::ActionEnded()
{
    UpdateActionStateOnEnd();

    OwnerCharacter->CurrentAction = nullptr;

    OnActionEndDelegate.Broadcast();
}

void UBaseAction::PlayAction()
{
    OwnerCharacter->CurrentAction = this;

    ActionState = EActionState::Executing;

    OnActionStartDelegate.Broadcast();
}

bool UBaseAction::CheckPreconditions()
{
    return ActionState == EActionState::Available && OwnerCharacter->IsValidLowLevel() && OwnerCharacter->StatsComponent->GetCurrentExclusiveState() != EMutuallyExclusiveState::Dead;
}

void UBaseAction::PostEnd()
{
    bTickEnabled = false;
}

void UBaseAction::PlayAnimation()
{
    if (UseDuration)
    {
        float DurationInSecs = Duration * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();
        float PlayRate = (AnimationAction->GetPlayLength() / AnimationAction->RateScale) / DurationInSecs;
        OwnerCharacter->PlayAnimMontage(AnimationAction, PlayRate);
    }
    else
    {
        OwnerCharacter->PlayAnimMontage(AnimationAction);
    }
}
