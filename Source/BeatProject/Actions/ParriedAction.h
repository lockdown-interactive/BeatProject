// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseStateAction.h"
#include "ParriedAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UParriedAction : public UBaseStateAction
{
	GENERATED_BODY()

public:
	UParriedAction();

	protected:
    virtual bool CheckPreconditions() override;

	virtual void PlayAction() override;
};
