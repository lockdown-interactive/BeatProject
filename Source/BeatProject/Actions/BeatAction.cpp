#include "BeatAction.h"
#include "BeatProject/Components/CombatComponent.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Managers/BeatManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "FMODAudioComponent.h"

UBeatAction::UBeatAction()
    : ActionValue(30.f)
    , LowAccuracyActionValue(30.f)
    , PenaltyCooldown(0)
    , BeforeOffsetMillisecs(100)
    , AfterOffsetMillisecs(100)
    , BeforeOffsetBeats(.1f)
    , AfterOffsetBeats(.1f)
{

}

void UBeatAction::InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager)
{
    Super::InitializeAction(BeatCharacter, WorldBeatManager);

    BeatManager = &WorldBeatManager;

    // Getting Before and After beat offset from Accuracy Data Table
    FComboAccuracyMultiplierRow* Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(TEXT("Poor"), TEXT(""));
    BeforeOffsetBeats = 1 - Row->Percentages.GetLowerBoundValue() * 0.01;
    Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(TEXT("Nice2"), TEXT(""));
    AfterOffsetBeats = 0.01 * Row->Percentages.GetUpperBoundValue() - 1;

    UpdateBPM(BeatManager->BPM);
    BeatManager->OnBMPChangedDelegate.AddDynamic(this, &UBeatAction::UpdateBPM);
}

bool UBeatAction::TryPlayAction()
{
    EBeatState CurrentBeatState = BeatManager->IsOnBeat(BeforeOffsetMillisecs, AfterOffsetMillisecs);

    int CurrentTimelinePosition = BeatManager->AudioComponent->GetTimelinePosition();
    int LastBeatAfterPositionLimit = BeatManager->LastBeatTimelinePosition + AfterOffsetMillisecs;

    int NextBeatTimelinePosition = BeatManager->LastBeatTimelinePosition + BeatManager->GetMillisecondsBetweenBeats();
    int NextBeatBeforePositionLimit = NextBeatTimelinePosition - BeforeOffsetMillisecs;

    EBeatAccuracy ActionAccuracy = GetActionAccuracy(CurrentTimelinePosition);
    if (Cast<APlayerController>(OwnerCharacter->GetController()))
    {
        BeatManager->AttemptBeatActionDelegate.Broadcast(this, CurrentTimelinePosition, LastBeatAfterPositionLimit, NextBeatBeforePositionLimit, ActionAccuracy);
    }

    if (BeatManager->UseBeatBufferRework) //Execute Beat action 
    {
        APlayerController* Controller = Cast<APlayerController>(OwnerCharacter->Controller);
        if (CurrentBeatState != EBeatState::OutBeat)
        {
            if (OwnerCharacter->StatsComponent->GetActionsInputEnable())
            {
                // Perform the action
                if (Super::TryPlayAction())
                {
                    if (Controller->IsValidLowLevel()) //Check if the character is a player
                    {
                        BeatManager->SetActionPlayedOnLastBeat(true);
                        BeatManager->OnPlayAction.Broadcast(this, ActionAccuracy);
                    }
                }
            }
        }
        else
        {
            //Action out of beat
            //Start out of beat penalty to avoid spamming beat action
            if (PenaltyCooldown > 0.f)
            {
                ActionState = EActionState::Cooldown;
                float PenaltySeconds = PenaltyCooldown * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();
                OwnerCharacter->GetWorldTimerManager().SetTimer(PenaltyCooldownTimerHandle, this, &UBeatAction::ResetPenaltyCooldown, PenaltySeconds, false);
            }

            OwnerCharacter->CombatComponent->ResetCombo();

            if (Controller->IsValidLowLevel()) //Check if the character is a player
            {
                BeatManager->SetActionPlayedOnLastBeat(false);
            }
        }
    }
    else
    {
        if (CurrentBeatState == EBeatState::Beat || CurrentBeatState == EBeatState::AfterBeat)
        {
            if (OwnerCharacter->StatsComponent->GetActionsInputEnable())
            {
                // Perform the action
                if (Super::TryPlayAction())
                {
                    BeatManager->OnPlayAction.Broadcast(this, ActionAccuracy);
                }
            }
        }
        else if (CurrentBeatState == EBeatState::BeforeBeat)
        {
            //If there are more than one actions in the same before offset -> Execute the last one
            if (OwnerCharacter->CurrentBeforeOffsetAction->IsValidLowLevel())
            {
                BeatManager->OnBeatDelegate.RemoveDynamic(OwnerCharacter->CurrentBeforeOffsetAction, &UBeatAction::OnTimelineBeat);
            }

            OwnerCharacter->CurrentBeforeOffsetAction = this;
            BeatManager->OnBeatDelegate.AddDynamic(OwnerCharacter->CurrentBeforeOffsetAction, &UBeatAction::OnTimelineBeat);
        }
        else
        {
            //Action out of beat
            //Start out of beat penalty to avoid spamming beat action
            if (PenaltyCooldown > 0.f)
            {
                ActionState = EActionState::Cooldown;
                float PenaltySeconds = PenaltyCooldown * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();
                OwnerCharacter->GetWorldTimerManager().SetTimer(PenaltyCooldownTimerHandle, this, &UBeatAction::ResetPenaltyCooldown, PenaltySeconds, false);
            }

            OwnerCharacter->CombatComponent->ResetCombo();
        }
    }

    return CurrentBeatState != EBeatState::OutBeat;
}

void UBeatAction::UpdateBPM(int BPM)
{
    BeforeOffsetMillisecs = BeatManager->GetMillisecondsBetweenBeats() * BeforeOffsetBeats;
    AfterOffsetMillisecs = BeatManager->GetMillisecondsBetweenBeats() * AfterOffsetBeats;
}

void UBeatAction::OnTimelineBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper,
    int32 TimeSignatureLower)
{
    TryPlayAction();
    OwnerCharacter->CurrentBeforeOffsetAction = nullptr;
    BeatManager->OnBeatDelegate.RemoveDynamic(this, &UBeatAction::OnTimelineBeat);
}

void UBeatAction::ResetPenaltyCooldown()
{
    ActionState = EActionState::Available;

    OwnerCharacter->GetWorldTimerManager().ClearTimer(PenaltyCooldownTimerHandle);
}

EBeatAccuracy UBeatAction::GetActionAccuracy(float CurrentTimelinePosition)
{
    float TimeAfterLastBeat = CurrentTimelinePosition - BeatManager->LastBeatTimelinePosition;
    float BeatPercent = FMath::Clamp<float>(100 * TimeAfterLastBeat / BeatManager->GetMillisecondsBetweenBeats(), 0, 100);

    if(GetRangeFromDataTable(TEXT("Poor")).Contains(BeatPercent))
    {
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, TEXT("poor"));
 
        OwnerCharacter->StatsComponent->SumCurrentGaugeValue(GetComboSumFromDataTable(TEXT("Poor")));
        OwnerCharacter->StatsComponent->SumComboStreak();
        OwnerCharacter->StatsComponent->SetCurrentActionScoreMul(GetScoreMultiplierFromDataTable(TEXT("Poor")));
        return EBeatAccuracy::Poor;
    }
    if(GetRangeFromDataTable(TEXT("Perfect1")).Contains(BeatPercent))
    {
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::White, TEXT("perfect1"));

        OwnerCharacter->StatsComponent->SumCurrentGaugeValue(GetComboSumFromDataTable(TEXT("Perfect1")));
        OwnerCharacter->StatsComponent->SumComboStreak();
        OwnerCharacter->StatsComponent->SetCurrentActionScoreMul(GetScoreMultiplierFromDataTable(TEXT("Perfect1")));
        return EBeatAccuracy::Perfect;
    }

    if (GetRangeFromDataTable(TEXT("Perfect2")).Contains(100 + BeatPercent))
    {
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::White, TEXT("perfect2"));

        OwnerCharacter->StatsComponent->SumCurrentGaugeValue(GetComboSumFromDataTable(TEXT("Perfect2")));
        OwnerCharacter->StatsComponent->SumComboStreak();
        OwnerCharacter->StatsComponent->SetCurrentActionScoreMul(GetScoreMultiplierFromDataTable(TEXT("Perfect2")));
        return EBeatAccuracy::Perfect;
    }

    if (GetRangeFromDataTable(TEXT("Nice1")).Contains(BeatPercent))
    {
       // GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Orange, TEXT("nice1"));

        OwnerCharacter->StatsComponent->SumCurrentGaugeValue(GetComboSumFromDataTable(TEXT("Nice1")));
        OwnerCharacter->StatsComponent->SumComboStreak();
        OwnerCharacter->StatsComponent->SetCurrentActionScoreMul(GetScoreMultiplierFromDataTable(TEXT("Nice1")));

        return EBeatAccuracy::Nice;
    }

    if (GetRangeFromDataTable(TEXT("Nice2")).Contains(100 + BeatPercent))
    {
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, TEXT("nice2"));

        OwnerCharacter->StatsComponent->SumCurrentGaugeValue(GetComboSumFromDataTable(TEXT("Nice2")));
        OwnerCharacter->StatsComponent->SumComboStreak();
        OwnerCharacter->StatsComponent->SetCurrentActionScoreMul(GetScoreMultiplierFromDataTable(TEXT("Nice2")));

        return EBeatAccuracy::Nice;
    }
    
    //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Fail"));

    OwnerCharacter->StatsComponent->ResetCurrentGaugeValue();
    OwnerCharacter->StatsComponent->ResetComboStreak();
    return EBeatAccuracy::Fail;
}

int UBeatAction::GetComboSumFromDataTable(FName CurrentAccuracy)
{
    FComboAccuracyMultiplierRow* Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(CurrentAccuracy, TEXT(""));
    if( Row )
    {
        return Row->ComboSum;
    }
    //UE_LOG(LogDataTable, Warning, TEXT("GetComboSumFromDataTable(): requested row '%s' not in DataTable"), CurrentAccuracy.ToString());
    return 0;
}

FFloatRange UBeatAction::GetRangeFromDataTable(FName Accuracy)
{
    FComboAccuracyMultiplierRow* Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(Accuracy, TEXT(""));
    if( Row )
    {
        return Row->Percentages;
    }
    //UE_LOG(LogDataTable, Warning, TEXT("GetRangeFromDataTable(): requested row '%s' not in DataTable"), Accuracy);
    return FFloatRange(0,0);
}

float UBeatAction::GetScoreMultiplierFromDataTable(FName Accuracy)
{
    FComboAccuracyMultiplierRow* Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(Accuracy, TEXT(""));
    if (Row)
    {
        return Row->ScoreMultiplier;
    }
    //UE_LOG(LogDataTable, Warning, TEXT("GetScoreMultiplierFromDataTable(): requested row '%s' not in DataTable"), Accuracy);
    return 0.f;
}
