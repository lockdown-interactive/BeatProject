// Fill out your copyright notice in the Description page of Project Settings.


#include "ParriedAction.h"
#include "BeatProject/BeatProjectCharacter.h"

UParriedAction::UParriedAction()
{
    MutuallyExclusiveState = EMutuallyExclusiveState::Parried;

}

bool UParriedAction::CheckPreconditions()
{
    return Super::CheckPreconditions();
}

void UParriedAction::PlayAction()
{
    Super::PlayAction();

    OwnerCharacter->EOnParryReceivedDelegate.Broadcast(OwnerCharacter);
}
