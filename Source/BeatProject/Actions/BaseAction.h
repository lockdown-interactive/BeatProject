// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseAction.generated.h"

#pragma region forward declaration
class ABeatManager;
class ABeatProjectCharacter;
class UBaseAction;
#pragma endregion

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnActionStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnActionEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActionInterrupt, UBaseAction*, InstigatorAction);

//Action substate
UENUM(BlueprintType)
enum class EActionState : uint8
{
    Available UMETA(DisplayName = "Available"),
    DoingAnticipation UMETA(DisplayName = "Doing Anticipation"),
    Executing UMETA(DisplayName = "Executing"),
    Cooldown UMETA(DisplayName = "Cooldown")
};

//Action Progress State
UENUM(BlueprintType)
enum class EProgressState : uint8
{
    PreStart UMETA(DisplayName = "Pre Start"),
    StartUpdate UMETA(DisplayName = "Start Update"),
    PostStart UMETA(DisplayName = "Post Start"),
    PreTick UMETA(DisplayName = "Pre Tick"),
    TickUpdate UMETA(DisplayName = "Tick Update"),
    PostTick UMETA(DisplayName = "Post Tick"),
    PreEnd UMETA(DisplayName = "Pre End"),
    EndUpdate UMETA(DisplayName = "End Update"),
    PostEnd UMETA(DisplayName = "Post End"),
    Cancel UMETA(DisplayName = "Cancel")
};

/**
 *
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class BEATPROJECT_API UBaseAction : public UObject, public FTickableGameObject
{
    GENERATED_BODY()

public:
    UBaseAction();

    /* BeatAction Animation*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|BaseAction", meta = (DisplayName = "Animation"))
        UAnimMontage* AnimationAction;

    // Duration in beats 
    UPROPERTY(EditAnywhere, Category = "Beat|BaseAction", meta = (DisplayName = "Use Duration"))
        bool UseDuration;

    // Duration in beats 
    UPROPERTY(EditAnywhere, Category = "Beat|BaseAction", meta = (DisplayName = "Duration", EditCondition = "UseDuration"))
        float Duration;

    /* Owner Character */
    UPROPERTY(BlueprintReadOnly, Category = "Beat|BaseAction", meta = (DisplayName = "Owner Character"))
        ABeatProjectCharacter* OwnerCharacter;

    //Event Delegates
    UPROPERTY(BlueprintAssignable, Category = "Beat|BeatAction|Delegates", meta = (DisplayName = "On Action Start Delegate"))
        FOnActionStart OnActionStartDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|BeatAction|Delegates", meta = (DisplayName = "On Action End Delegate"))
        FOnActionEnd OnActionEndDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|BeatAction|Delegates", meta = (DisplayName = "On Action Interrupt Delegate"))
        FOnActionInterrupt OnActionInterruptDelegate;


protected:
    bool bTickEnabled;

    UPROPERTY()
        EProgressState CurrentProgressState;

    UPROPERTY(VisibleAnywhere, meta = (DisplayName = "Action State"))
        EActionState ActionState;

public:
    virtual void InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager);

    UFUNCTION(BlueprintCallable)
    virtual bool TryPlayAction();

    virtual void Tick(float DeltaTime) override;
    virtual bool IsTickable() const override;
    virtual TStatId GetStatId() const override;

    UFUNCTION(BlueprintCallable)
        EProgressState GetCurrentProgressState();

    UFUNCTION(BlueprintCallable)
        EActionState GetActionState();

    UFUNCTION(BlueprintCallable)
        virtual void Cancel(UBaseAction* InstigatorAction);
    void UpdateActionStateOnEnd();

    UFUNCTION()
        virtual void ActionEnded();

protected:
    UFUNCTION()
        virtual void PlayAction();

    UFUNCTION()
        virtual bool CheckPreconditions();

    /*
        Start : Use it to do stuff that you need to do before the action, but it isn't an action (aim assist)
        Tick : Use it to do the action itself (All displacements, animations and things that are done while doing the action)
        End : Use it to do stuff that you need to do after the action, but it isn't an action (push enemies to avoid be inside them when doing the dash)
    */

    //    Add in the first start function:
    //    bTickEnabled = true;
    //    OnActionStartDelegate.Broadcast();

    UFUNCTION()
        virtual void PreStart() PURE_VIRTUAL(UTickableBeatAction::PreStart, );
    UFUNCTION()
        virtual void StartUpdate(float DeltaTime) PURE_VIRTUAL(UTickableBeatAction::StartUpdate, );
    UFUNCTION()
        virtual void PostStart() PURE_VIRTUAL(UTickableBeatAction::PostStart, );

    UFUNCTION()
        virtual void PreTick() PURE_VIRTUAL(UTickableBeatAction::PreTick, );
    UFUNCTION()
        virtual void TickUpdate(float DeltaTime) PURE_VIRTUAL(UTickableBeatAction::TickUpdate, );
    UFUNCTION()
        virtual void PostTick() PURE_VIRTUAL(UTickableBeatAction::PostTick, );

    UFUNCTION()
        virtual void PreEnd() PURE_VIRTUAL(UTickableBeatAction::PreEnd, );
    UFUNCTION()
        virtual void EndUpdate(float DeltaTime) PURE_VIRTUAL(UTickableBeatAction::EndUpdate, );
    UFUNCTION()
        virtual void PostEnd();

    void PlayAnimation();
};
