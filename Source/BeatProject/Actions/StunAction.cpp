// Fill out your copyright notice in the Description page of Project Settings.


#include "StunAction.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"


UStunAction::UStunAction()
{
    MutuallyExclusiveState = EMutuallyExclusiveState::Stun;
}

bool UStunAction::CheckPreconditions()
{
    return Super::CheckPreconditions();
}
