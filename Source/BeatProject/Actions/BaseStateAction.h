// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseAction.h"
#include "BeatProject/BeatProject.h"

#include "BaseStateAction.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class BEATPROJECT_API UBaseStateAction : public UBaseAction
{
	GENERATED_BODY()


public:
    UBaseStateAction();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Beat|BeatAction")
    EMutuallyExclusiveState MutuallyExclusiveState;

public:
	virtual void Cancel(UBaseAction* InstigatorAction) override;
    virtual void ActionEnded() override;

protected:
    virtual void PlayAction() override;
    virtual bool CheckPreconditions() override;

    /*
     In case you only use one Start or one End use the POST
     */

     //    Add in the first start function:
     //    bTickEnabled = true;
     //    OnActionStartDelegate.Broadcast();

    virtual void PreStart();
    virtual void StartUpdate(float DeltaTime);
    virtual void PostStart() override;

    virtual void PreTick();
    virtual void TickUpdate(float DeltaTime);
    virtual void PostTick();

    virtual void PreEnd();
    virtual void EndUpdate(float DeltaTime);
    virtual void PostEnd() override;
};
