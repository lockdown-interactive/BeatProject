// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BeatAction.h"
#include "JumpAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UJumpAction : public UBeatAction
{
	GENERATED_BODY()

public:
    UJumpAction();

    /* Jump distance in Z axis*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|Jump", meta = (DisplayName = "JumpZDistance"))
        float JumpZDistance;

    float gravity;
	
public:
    virtual void InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager) override;

protected:
    virtual void PlayAction() override;

    virtual bool CheckPreconditions() override;

private:
    void SetUpJump();

};
