// Fill out your copyright notice in the Description page of Project Settings.


#include "ParryAction.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Components/CombatComponent.h"
#include "BeatProject/Core/Attack.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

UParryAction::UParryAction()
{
    MutuallyExclusiveState = EMutuallyExclusiveState::Parry;
}

void UParryAction::PlayAction()
{
    Super::PlayAction();

    OwnerCharacter->EOnParryDelegate.Broadcast(OwnerCharacter);
}

void UParryAction::PostEnd()
{
    Super::PostEnd();

    AAIController* ParryController = Cast<AAIController>(OwnerCharacter->GetController());
    if (ParryController) //Enemy, stop defense behavior
    {
        ParryController->GetBlackboardComponent()->SetValueAsBool(FName("OnDefense"), false);
    }
}
