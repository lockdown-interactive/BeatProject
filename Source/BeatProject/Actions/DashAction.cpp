// Fill out your copyright notice in the Description page of Project Settings.

#include "DashAction.h"

#include "DrawDebugHelpers.h"
#include "BeatProject/BeatProject.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Managers/BeatManager.h"
#include "Components/CapsuleComponent.h"
#include "Components/TimelineComponent.h"
#include "BeatProject/Components/StatsComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

UDashAction::UDashAction()
    : FloatCurve(nullptr)
    , Distance(350.f)
    , DisplacementOffset(100.f)
    , AirDashLaunchForce(500.f)
{
    Duration = 0.6f;
}

void UDashAction::InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager)
{
    Super::InitializeAction(BeatCharacter, WorldBeatManager);

    FOnTimelineFloat ProgressFunction;
    ProgressFunction.BindUFunction(this, FName("HandleProgress"));

    if (FloatCurve)
    {
        //Setting up the loop status and the function that is going to fire when the timeline ticks
        Timeline.AddInterpFloat(FloatCurve, ProgressFunction);
    }
}

void UDashAction::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void UDashAction::PlayAction()
{
    Super::PlayAction();

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::Dash);

    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.RemoveDynamic(this, &UDashAction::OnDashCapsuleBeginOverlap);
    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &UDashAction::OnDashCapsuleBeginOverlap);

    OwnerCharacter->OnDashStartDelegate.Broadcast();
    CurrentProgressState = EProgressState::PostStart;
    PostStart();
}

void UDashAction::PostStart()
{
    //Always call tick enabled before SetUpDash to avoid call cancel before setting btickenabled to true
    //Cancel could be called instantly after enabling dash capsule collision
    bTickEnabled = true;
    CurrentProgressState = EProgressState::TickUpdate;
    SetUpDash();

    float DurationInSecs = Duration * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();

    PlayAnimation();

    Timeline.SetTimelineLength(DurationInSecs);
    Timeline.SetTimelineLengthMode(ETimelineLengthMode::TL_TimelineLength);
    //Starting the timeline...
    Timeline.PlayFromStart();

    UE_LOG(LogTemp, Warning, TEXT("Initial Position: , %s"), *(InitialPosition.ToString()));
}

void UDashAction::TickUpdate(float DeltaTime)
{
    //Advancing the timeline in every tick
    Timeline.TickTimeline(DeltaTime);

    FVector TargetPosition = FMath::Lerp(InitialPosition, FinalPosition, CurrentCurveValue);
    FVector VelToTarget = (TargetPosition - OwnerCharacter->GetActorLocation()) / DeltaTime;

    OwnerCharacter->GetMovementComponent()->Velocity = VelToTarget;

    if (CurrentCurveValue >= 1.f)
    {
        CurrentProgressState = EProgressState::PreEnd;
    }
}

void UDashAction::PreEnd()
{
    // draw collision sphere
    //DrawDebugCapsule(OwnerCharacter->GetWorld(), FinalPosition, OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius(), FQuat::Identity, FColor::Purple, true);

    // check if something got hit in the sweep
    FHitResult OutHit;
    FCollisionShape Capsule = FCollisionShape::MakeCapsule(OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius());
    bool Hit = OwnerCharacter->GetWorld()->SweepSingleByChannel(OutHit, FinalPosition, FinalPosition, FQuat::Identity, ECC_DASH_TRACE_CHANNEL, Capsule);

    if (!Hit && !OwnerCharacter->GetMovementComponent()->IsFalling())
    {
        FinalPosition.Z = OwnerCharacter->GetActorLocation().Z;
        OwnerCharacter->SetActorLocation(FinalPosition);
    }

    UE_LOG(LogTemp, Warning, TEXT("End Position: , %s"), *(OwnerCharacter->GetActorLocation().ToString()));

    FinalPosition = OwnerCharacter->GetActorLocation();

    //Check if any enemy is on the final position
    FCollisionQueryParams Params;
    Params.AddIgnoredActor(OwnerCharacter);

    TArray<FHitResult> OutHits;
    Hit = OwnerCharacter->GetWorld()->SweepMultiByChannel(OutHits, FinalPosition, FinalPosition, FQuat::Identity, ECC_Pawn, Capsule, Params);

    if (Hit)
    {
        for (FHitResult HitResult : OutHits)
        {
            ABeatProjectCharacter* HitCharacter = Cast<ABeatProjectCharacter>(HitResult.GetActor());
            if (HitCharacter->IsValidLowLevel())
            {
                FEnemyDisplacement EnemyDisplacement;
                EnemyDisplacement.Enemy = HitCharacter;
                EnemyDisplacement.CurrentPosition = HitCharacter->GetActorLocation();

                //Get Displacement direction
                EnemyDisplacement.DisplacementDirection = HitCharacter->GetActorLocation() - OwnerCharacter->GetActorLocation();
                EnemyDisplacement.DisplacementDirection = EnemyDisplacement.DisplacementDirection.GetSafeNormal();

                //Get Displacement Distance
                EnemyDisplacement.DisplacementDistance = HitCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius() + OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius() - HitCharacter->GetDistanceTo(OwnerCharacter) + DisplacementOffset;

                EnemyDisplacement.FinalPosition = HitCharacter->GetActorLocation() + EnemyDisplacement.DisplacementDirection * EnemyDisplacement.DisplacementDistance;

                //Deactivate Friction on Displacement
                EnemyDisplacement.Enemy->GetCharacterMovement()->BrakingFrictionFactor = 0.f;
                EnemyDisplacement.Enemy->GetCharacterMovement()->GravityScale = 0.f;

                DisplacementEnemies.AddUnique(EnemyDisplacement);
            }
        }

        CurrentProgressState = EProgressState::EndUpdate;
    }
    else
    {
        CurrentProgressState = EProgressState::PostEnd;
    }
}

void UDashAction::EndUpdate(float DeltaTime)
{
    bool AllReached = true;

    for (FEnemyDisplacement EnemyDisplacement : DisplacementEnemies)
    {
        if (!EnemyDisplacement.TargetReached)
        {
            EnemyDisplacement.Enemy->GetMovementComponent()->Velocity = EnemyDisplacement.DisplacementDirection * EnemyDisplacement.Enemy->OverlapDisplacementVelocity;

            EnemyDisplacement.TargetReached = FVector::Distance(EnemyDisplacement.Enemy->GetActorLocation(), OwnerCharacter->GetActorLocation()) > EnemyDisplacement.DisplacementDistance;
            if (!EnemyDisplacement.TargetReached)
            {
                AllReached = false;
            }
        }
    }

    if (AllReached)
    {
        CurrentProgressState = EProgressState::PostEnd;
    }
}

void UDashAction::PostEnd()
{
    Super::PostEnd();
    FinishDash();

    //Deactivate Friction on Displacement
    for (FEnemyDisplacement EnemyDisplacement : DisplacementEnemies)
    {
        EnemyDisplacement.Enemy->GetCharacterMovement()->BrakingFrictionFactor = EnemyDisplacement.Enemy->DefaultFrictionFactor;
        EnemyDisplacement.Enemy->GetCharacterMovement()->GravityScale = EnemyDisplacement.Enemy->DefaultGravityScale;
    }
    DisplacementEnemies.Empty();

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::IdleRun);

    OwnerCharacter->OnDashEndDelegate.Broadcast();
}

bool UDashAction::CheckPreconditions()
{
     return Super::CheckPreconditions() && OwnerCharacter->StatsComponent->GetActionsInputEnable();
}

void UDashAction::SetUpDash()
{
    //Deactivate Friction on Dash
    OwnerCharacter->GetCharacterMovement()->BrakingFrictionFactor = 0.f;
    OwnerCharacter->GetCharacterMovement()->GravityScale = 0.f;

    //Setup Dash Capsule
    OwnerCharacter->GetMovementCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    OwnerCharacter->GetMovementCapsuleComponent()->SetUseCCD(true);

    //Set character capsule collision with Pawns to Overlap
    OwnerCharacter->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

    //Calculate positions
    InitialPosition = OwnerCharacter->GetActorLocation();

    FinalPosition = InitialPosition + OwnerCharacter->GetPlayerLastInputOrForward() * Distance;

    CurrentCurveValue = 0;
}

void UDashAction::RestoreDash()
{
    OwnerCharacter->GetCharacterMovement()->BrakingFrictionFactor = OwnerCharacter->DefaultFrictionFactor;
    OwnerCharacter->GetCharacterMovement()->GravityScale = OwnerCharacter->DefaultGravityScale;

    OwnerCharacter->GetMovementCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    OwnerCharacter->GetMovementCapsuleComponent()->SetUseCCD(false);

    OwnerCharacter->GetMovementCapsuleComponent()->OnComponentBeginOverlap.RemoveDynamic(this, &UDashAction::OnDashCapsuleBeginOverlap);
    
    //Restore character capsule collision
    OwnerCharacter->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
}

void UDashAction::HandleProgress(float Value)
{
    float DurationInSecs = Duration * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();

    CurrentCurveValue = FloatCurve->GetFloatValue(Timeline.GetPlaybackPosition() / DurationInSecs);
}

void UDashAction::Cancel(UBaseAction* InstigatorAction)
{
    FinishDash();
    OwnerCharacter->OnDashInterruptDelegate.Broadcast();

    //Always try to call at the end
    Super::Cancel(this);
}

void UDashAction::FinishDash()
{
    OwnerCharacter->GetMovementComponent()->Velocity = FVector::ZeroVector;

    if (OwnerCharacter->GetMovementComponent()->IsFalling())
    {
        OwnerCharacter->LaunchCharacter(OwnerCharacter->GetPlayerLastInputOrForward() * AirDashLaunchForce, true, true);
    }

    RestoreDash();
}

void UDashAction::OnDashCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    FinishDash();
}