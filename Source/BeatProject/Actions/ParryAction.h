// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseStateAction.h"
#include "ParryAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UParryAction : public UBaseStateAction
{
	GENERATED_BODY()

public:
	UParryAction();

	protected:

	virtual void PlayAction() override;
	
    virtual void PostEnd() override;
};
