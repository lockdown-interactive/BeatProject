// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseStateAction.h"
#include "StunAction.generated.h"

/**
 * 
 */
UCLASS()
class BEATPROJECT_API UStunAction : public UBaseStateAction
{
	GENERATED_BODY()

public:
    UStunAction();

protected:
    virtual bool CheckPreconditions() override;
};
