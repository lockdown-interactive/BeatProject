// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BeatAction.h"
#include "Components/TimelineComponent.h"

#include "DashAction.generated.h"

USTRUCT()
struct FEnemyDisplacement
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Enemy"))
        ABeatProjectCharacter* Enemy;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Initial Position"))
        FVector CurrentPosition;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Final Position"))
        FVector FinalPosition;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Displacement Direction"))
        FVector DisplacementDirection;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Displacement Distance"))
        float DisplacementDistance;

    UPROPERTY(VisibleAnywhere, Category = "Beat|Displacement", meta = (DisplayName = "Target Reached"))
        bool TargetReached = false;

    FORCEINLINE bool operator==(const FEnemyDisplacement& Other) const { return Enemy == Other.Enemy; }
};

/**
 *
 */
UCLASS()
class BEATPROJECT_API UDashAction : public UBeatAction
{
    GENERATED_BODY()

public:
    UDashAction();

    //Curve for Dash
    UPROPERTY(EditAnywhere, Category = "Beat|Dash", meta = (DisplayName = "Dash Curve"))
        UCurveFloat* FloatCurve;

    UPROPERTY(EditAnywhere, Category = "Beat|Dash", meta = (DisplayName = "Dash Distance"))
        float Distance;

    // Offset to displace enemies when overlapping after dash
    UPROPERTY(EditAnywhere, Category = "Beat|Dash", meta = (DisplayName = "Displacement Offset"))
        float DisplacementOffset;

    // Launch Force on air dash ends
    UPROPERTY(EditAnywhere, Category = "Beat|Dash", meta = (DisplayName = "Air Dash Launch Force"))
        float AirDashLaunchForce;

private:

    FTimeline Timeline;
    float CurrentCurveValue;

    FVector InitialPosition;
    FVector FinalPosition;

    TArray<FEnemyDisplacement> DisplacementEnemies;

public:
    virtual void InitializeAction(ABeatProjectCharacter& BeatCharacter, ABeatManager& WorldBeatManager) override;
    virtual void Tick(float DeltaTime) override;
    virtual void Cancel(UBaseAction* InstigatorAction) override;

protected:
    virtual void PlayAction() override;
    virtual void PreStart() override {}
    virtual void StartUpdate(float DeltaTime) override {}
    virtual void PostStart() override;
    virtual void PreTick() override {}
    virtual void TickUpdate(float DeltaTime) override;
    virtual void PostTick() override {}
    virtual void PreEnd() override;
    virtual void EndUpdate(float DeltaTime) override;
    virtual void PostEnd() override;

    virtual bool CheckPreconditions() override;

private:
    void SetUpDash();
    void RestoreDash();

    UFUNCTION()
        void HandleProgress(float Value);

    void FinishDash();

    UFUNCTION()
        void OnDashCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
            UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
