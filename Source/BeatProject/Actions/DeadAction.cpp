// Fill out your copyright notice in the Description page of Project Settings.


#include "DeadAction.h"

#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "Kismet/GameplayStatics.h"

UDeadAction::UDeadAction()
{
    MutuallyExclusiveState = EMutuallyExclusiveState::Dead;
}

bool UDeadAction::CheckPreconditions()
{
    return Super::CheckPreconditions() && OwnerCharacter->StatsComponent->GetCurrentExclusiveState() != EMutuallyExclusiveState::Dead;
}

void UDeadAction::PostEnd()
{
    Super::PostEnd();

    if(Cast<APlayerController>(OwnerCharacter->GetController()))
    {
        UGameplayStatics::OpenLevel(GetWorld(), "Blockout_A");
    }
    else
    {
        OwnerCharacter->Destroy();
    }
}