// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseStateAction.h"

#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "BeatProject/Managers/BeatManager.h"


UBaseStateAction::UBaseStateAction()
{

}

void UBaseStateAction::Cancel(UBaseAction* InstigatorAction)
{
    Super::Cancel(InstigatorAction);
}

void UBaseStateAction::PlayAction()
{
    Super::PlayAction();

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(MutuallyExclusiveState);

    PostStart();
}

bool UBaseStateAction::CheckPreconditions()
{
    return Super::CheckPreconditions();
}

void UBaseStateAction::PreStart()
{

}

void UBaseStateAction::StartUpdate(float DeltaTime)
{

}

void UBaseStateAction::PostStart()
{
    CurrentProgressState = EProgressState::TickUpdate;
    bTickEnabled = true;

    PlayAnimation();

    OnActionStartDelegate.Broadcast();
}

void UBaseStateAction::PreTick()
{

}

void UBaseStateAction::TickUpdate(float DeltaTime)
{

}

void UBaseStateAction::PostTick()
{

}

void UBaseStateAction::PreEnd()
{

}

void UBaseStateAction::EndUpdate(float DeltaTime)
{

}

void UBaseStateAction::PostEnd()
{
    Super::PostEnd();

    OwnerCharacter->StatsComponent->SetCurrentExclusiveState(EMutuallyExclusiveState::IdleRun);
}

void UBaseStateAction::ActionEnded()
{
    PostEnd();

    Super::ActionEnded();
}
