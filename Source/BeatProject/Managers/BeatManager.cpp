// Fill out your copyright notice in the Description page of Project Settings.


#include "BeatManager.h"
#include "FMODAudioComponent.h"

#include "BeatProject/BeatProject.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Components/StatsComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABeatManager::ABeatManager()
    : UseBeatBufferRework(false)
    , BPM(90)
    , LastBeatIndex(0)
    , LastBeatTimelinePosition(0)
    , ActionPlayedOnLastBeat(false)
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    AudioComponent = CreateDefaultSubobject<UFMODAudioComponent>(TEXT("FMODAudioComponent"));
    AudioComponent->OnTimelineBeat.AddDynamic(this, &ABeatManager::OnTimelineBeat);
    AudioComponent->bEnableTimelineCallbacks = true;
}

// Called when the game starts or when spawned
void ABeatManager::BeginPlay()
{
    Super::BeginPlay();

    AudioComponent->Play();

    OwnerCharacter = Cast<ABeatProjectCharacter>( UGameplayStatics::GetPlayerCharacter(GetWorld(), 0) );

    // Getting Before and After beat offset from Accuracy Data Table
    FComboAccuracyMultiplierRow* Row = OwnerCharacter->StatsComponent->ComboAccuracyDataTable->FindRow<FComboAccuracyMultiplierRow>(TEXT("Nice2"), TEXT(""));
    float AfterOffsetBeats = 0.01 * Row->Percentages.GetUpperBoundValue() - 1;

    AfterBeatOffset = GetSecondsBetweenBeats()* AfterOffsetBeats;
}

// Called every frame
void ABeatManager::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABeatManager::OnTimelineBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper,
    int32 TimeSignatureLower)
{
    if (Tempo != BPM)
    {
        BPM = Tempo;
        OnBMPChangedDelegate.Broadcast(BPM);
    }

    LastBeatTimelinePosition = AudioComponent->GetTimelinePosition();
    LastBeatIndex = Beat;

    GetWorldTimerManager().SetTimer(AfterOffsetTimerHandle, this, &ABeatManager::ResetAfterOffsetTimer, AfterBeatOffset, false);

    OnBeatDelegate.Broadcast(Bar, Beat, LastBeatTimelinePosition, Tempo, TimeSignatureUpper, TimeSignatureLower);
}

EBeatState ABeatManager::IsOnBeat(float BeforeOffset, float AfterOffset)
{
    //Current song ms
    int CurrentTimelinePosition = AudioComponent->GetTimelinePosition();
    int LastBeatAfterPositionLimit = LastBeatTimelinePosition + AfterOffset;

    int NextBeatTimelinePosition = LastBeatTimelinePosition + GetMillisecondsBetweenBeats();
    int NextBeatAfterPositionLimit = NextBeatTimelinePosition - BeforeOffset;

    //	  beforeOffset   beat   afterOffset
    // ---------(         |          )---------
    //
    if (CurrentTimelinePosition == LastBeatAfterPositionLimit)
    {
        //On current beat
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("ON Beat"));
        return EBeatState::Beat;
    }
    else if (CurrentTimelinePosition < LastBeatAfterPositionLimit)
    {
        //On current beat after offset
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("After"));
        return EBeatState::AfterBeat;
    }
    else if (CurrentTimelinePosition > NextBeatAfterPositionLimit)
    {
        //On next beat before offset
        //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("Before"));
        return EBeatState::BeforeBeat;
    }
    //GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("OUT"));
    return EBeatState::OutBeat;
}

int ABeatManager::GetMillisecondsBetweenBeats() const
{
    return MINUTES_TO_MILLISECONDS / BPM;
}

float ABeatManager::GetSecondsBetweenBeats() const
{
    return MINUTES_TO_SECONDS / BPM;
}

void ABeatManager::ResetAfterOffsetTimer()
{
    if (!ActionPlayedOnLastBeat)
    {
        OnLostBeatDelegate.Broadcast();
    }

    ActionPlayedOnLastBeat = false;

    GetWorldTimerManager().ClearTimer(AfterOffsetTimerHandle);
}

void ABeatManager::SetActionPlayedOnLastBeat(bool ActionPlayed)
{
    ActionPlayedOnLastBeat = ActionPlayed;
}
