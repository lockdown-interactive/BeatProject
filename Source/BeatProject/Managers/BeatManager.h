// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BeatProject/BeatProject.h"
#include "GameFramework/Actor.h"
#include "BeatManager.generated.h"

class ABeatProjectCharacter;
class UFMODAudioComponent;
class UBeatAction;

#pragma region Delegate
/** called when we reach a beat on the timeline */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(
FOnBeat, int32, Bar, int32, Beat, int32, Position, float, Tempo, int32, TimeSignatureUpper, int32, TimeSignatureLower);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBMPChanged, int, BPM);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPlayAction, UBeatAction*, BeatAction, EBeatAccuracy, BeatAccuracy);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLostBeat);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FAttemptBeatAction, UBeatAction*, Action, float, CurrentTimelinePosition, float, LastBeatAfterPositionLimit, float, NextBeatAfterPositionLimit, EBeatAccuracy, ActionAccuracy);
#pragma endregion 

UCLASS()
class BEATPROJECT_API ABeatManager : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ABeatManager();

    /*FMOD Audio Component*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BeatManager", meta = (DisplayName = "FMODAudioComponent"))
        UFMODAudioComponent* AudioComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BeatManager", meta = (DisplayName = "Use Beat Buffer Rework"))
        bool UseBeatBufferRework;

    /*Beats per Minute*/
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BeatManager", meta = (DisplayName = "BPM"))
        int BPM;

    /*Current Beat Index*/
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BeatManager", meta = (DisplayName = "CurrentBeatIndex"))
        int LastBeatIndex;

    /*Current Timeline Position in song */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BeatManager", meta = (DisplayName = "CurrentTimeLinePosition"))
        int LastBeatTimelinePosition;

    #pragma region EventDelegates
    // CurrentTimelinePosition, LastBeatAfterPositionLimit, NextBeatBeforePositionLimit
    UPROPERTY(BlueprintAssignable, Category = "Beat|BeatAction|Delegates", meta = (DisplayName = "On Attempt Beat Action"))
    FAttemptBeatAction AttemptBeatActionDelegate;

     UPROPERTY(BlueprintAssignable, Category = "BeatManager", meta = (DisplayName = "On Beat"))
        FOnBeat OnBeatDelegate;

    UPROPERTY(BlueprintAssignable, Category = "BeatManager", meta = (DisplayName = "On Lost Beat"))
        FOnLostBeat OnLostBeatDelegate;

    UPROPERTY(BlueprintAssignable, Category = "BeatManager", meta = (DisplayName = "On BPM Changed"))
        FOnBMPChanged OnBMPChangedDelegate;

    UPROPERTY(BlueprintAssignable, Category = "BeatManager", meta = (DisplayName = "On Play Action"))
        FOnPlayAction OnPlayAction;

    #pragma endregion

private:
    UPROPERTY()
        ABeatProjectCharacter* OwnerCharacter;

    FTimerHandle AfterOffsetTimerHandle;
    bool ActionPlayedOnLastBeat;
    float AfterBeatOffset;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UFUNCTION()
        void OnTimelineBeat(int32 Bar, int32 Beat, int32 Position, float Tempo, int32 TimeSignatureUpper, int32 TimeSignatureLower);

    UFUNCTION(BlueprintCallable)
        EBeatState IsOnBeat(float BeforeOffset, float AfterOffset);

    UFUNCTION(BlueprintCallable)
        int GetMillisecondsBetweenBeats() const;

    UFUNCTION(BlueprintCallable)
        float GetSecondsBetweenBeats() const;

    UFUNCTION()
    void ResetAfterOffsetTimer();

    void SetActionPlayedOnLastBeat(bool ActionPlayed);
};
