// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BeatProject/BeatProject.h"
#include "Components/ActorComponent.h"
#include "AbilityComponent.generated.h"

#pragma region ForwardDeclaration
class UBaseAction;
class ABeatManager;
class ABeatProjectCharacter;
#pragma endregion 

UCLASS(ClassGroup = (Custom))
class BEATPROJECT_API UAbilityComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UAbilityComponent();

    // Cooldown in beats 
    UPROPERTY(EditAnywhere, Category = "Beat", meta = (DisplayName = "Cooldown"))
        float Cooldown;

protected:
    UPROPERTY()
    ABeatProjectCharacter* OwnerCharacter;

    UPROPERTY()
    ABeatManager* BeatManager;

private:

    /* Handle to manage the timer */
    FTimerHandle CooldownTimerHandle;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UFUNCTION()
        virtual bool CheckPreconditions();

    UFUNCTION()
        virtual void Cancel(UBaseAction* InstigatorAction) PURE_VIRTUAL(UAbilityComponent::Cancel, );
    
    UFUNCTION()
        virtual bool ExecuteAbility() PURE_VIRTUAL(UAbilityComponent::ExecuteAbility, return false;);

public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable)
        bool TryExecuteAbility();
};
