// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilityComponent.h"
#include "CoreMinimal.h"

#include "CombatComponent.generated.h"

#pragma region Forward declarations
class UBaseAttackAction;
class UBaseAction;
class UAttack;
#pragma endregion 

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BEATPROJECT_API UCombatComponent : public UAbilityComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UCombatComponent();

#pragma region AttackActions
    /* Actions Must Be Placed in order of execution */
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Beat|CombatComponent", meta = (DisplayName = "Action List BP"))
        TArray<TSubclassOf<UAttack>> ActionListBP;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|CombatComponent", meta = (DisplayName = "Action List"))
        TArray<UAttack*> ActionList;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Beat|CombatComponent", meta = (DisplayName = "Current Action List Index"))
        int CurrentActionListIndex;
#pragma endregion

#pragma region Defense
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Beat|CombatComponent|Defense", meta = (DisplayName = "Block"))
        TSubclassOf<UBaseAttackAction> BlockBP;

    UPROPERTY()
        UBaseAttackAction* Block;
#pragma endregion

#pragma region ResetCombo
    // Reset Combo Time in beats
    UPROPERTY(EditAnywhere, Category = "Beat|CombatComponent", meta = (DisplayName = "Reset Combo Time in beats", ClampMin = "0"))
        float ResetComboBeats;

    /* Handle to manage the timer */
    UPROPERTY()
        FTimerHandle ResetComboTimer;
#pragma endregion 

    UPROPERTY(BlueprintReadOnly, Category = "Beat|CombatComponent", meta = (DisplayName = "Current Beat Action"))
        UAttack* CurrentAttack;

private:

protected:

    // Called when the game starts
    virtual void BeginPlay() override;


public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable)
        void AttackOnBeat();

    UFUNCTION()
        void BlockOnBeat();

    UFUNCTION()
        void UpdateCurrentAction();

    UFUNCTION()
        void ResetCombo();

    UFUNCTION()
        void ActivateResetComboTimer();

protected:
    virtual bool CheckPreconditions() override;
    virtual void Cancel(UBaseAction* InstigatorAction) override;
    virtual bool ExecuteAbility() override;
};
