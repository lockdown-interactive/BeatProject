// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatComponent.h"

#include "DrawDebugHelpers.h"
#include "BeatProject/Managers/BeatManager.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "Kismet/GameplayStatics.h"
#include "BeatProject/Actions/BeatAction.h"
#include "BeatProject/Core/Attack.h"
#include "BeatProject/Actions/BaseAttackAction.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UCombatComponent::UCombatComponent()
    : CurrentActionListIndex(0)
{
    PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UCombatComponent::BeginPlay()
{
    Super::BeginPlay();

    OwnerCharacter = Cast<ABeatProjectCharacter>(GetOwner());
    BeatManager = Cast<ABeatManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABeatManager::StaticClass()));

    if (BlockBP->IsValidLowLevel())
    {
        Block = NewObject<UBaseAttackAction>(this, BlockBP, NAME_None, RF_NoFlags, BlockBP->GetDefaultObject(), true);
        Block->InitializeAction(*OwnerCharacter, *BeatManager);
    }

    for (int ActionIndex = 0; ActionIndex < ActionListBP.Num(); ++ActionIndex)
    {
        UAttack* BeatAction = NewObject<UAttack>(this, ActionListBP[ActionIndex], NAME_None, RF_NoFlags, ActionListBP[ActionIndex]->GetDefaultObject(), true);
        ActionList.Add(BeatAction);
        ActionList[ActionIndex]->Initialize(*OwnerCharacter, *BeatManager);
    }

    CurrentAttack = ActionList[0];
}


// Called every frame
void UCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UCombatComponent::AttackOnBeat()
{
    // Get the current action to perform
    CurrentAttack = ActionList[CurrentActionListIndex];

    // Check if I am on beat to perform the action
    if (CurrentAttack->IsValidLowLevel())
    {
        TryExecuteAbility();
    }
}

void UCombatComponent::BlockOnBeat()
{
    // Get the current action to perform
    CurrentAttack = nullptr;

    if (Block->IsValidLowLevel())
    {
        TryExecuteAbility();
    }
}

void UCombatComponent::ResetCombo()
{
    CurrentActionListIndex = 0;
    OwnerCharacter->GetWorldTimerManager().ClearTimer(ResetComboTimer);
}

void UCombatComponent::ActivateResetComboTimer()
{
    //Combo Reset time
    OwnerCharacter->GetWorldTimerManager().ClearTimer(ResetComboTimer);
    float ResetComboSeconds = ResetComboBeats * OwnerCharacter->BeatManager->GetSecondsBetweenBeats();
    OwnerCharacter->GetWorldTimerManager().SetTimer(ResetComboTimer, this, &UCombatComponent::ResetCombo, ResetComboSeconds, false);
}

void UCombatComponent::UpdateCurrentAction()
{
    if (OwnerCharacter->IsValidLowLevel() && CurrentAttack->IsValidLowLevel())
    {
        CurrentAttack = nullptr;

        if (CurrentActionListIndex >= ActionList.Num() - 1)
        {
            ResetCombo();
        }
        else
        {
            CurrentActionListIndex++;
        }
    }
}

bool UCombatComponent::CheckPreconditions()
{
    if (Super::CheckPreconditions())
    {
        return true;
    }

    return false;
}

void UCombatComponent::Cancel(UBaseAction* InstigatorAction)
{
    if (CurrentAttack->CurrentAction->IsValidLowLevel() && CurrentAttack->CurrentAction->GetActionState() == EActionState::Executing)
    {
        CurrentAttack->CurrentAction->Cancel(InstigatorAction);
    }
}

bool UCombatComponent::ExecuteAbility()
{
    if(CurrentAttack)
    {
        return CurrentAttack->TryPlayAction();
    }
    else
    {
        return Block->TryPlayAction();
    }
}

