// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BeatProject/BeatProject.h"
#include "Engine/DataTable.h"
#include "StatsComponent.generated.h"


class UBeatAction;
class ABeatManager;
class UNumericStat;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLifeChange, float, newLifeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnComboIndexUp, int, Multiplier);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnComboIndexDown, int, Multiplier);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAudienceIncrease, UBeatAction*, Action, float, Amount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAudienceDecrease, UBeatAction*, Action, float, Amount);

/**
 * Stats which the character can have active at the same time
 */
USTRUCT(BlueprintType)
struct FNonExclusiveStates
{
    GENERATED_USTRUCT_BODY()

        /** Checks if the character is invulnerable */
        UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Beat|Non Exclusive States")
        bool Invulnerable = false;

    /** Checks if the character is immovable, can't be affected by knockbacks or others external displacements */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Beat|Non Exclusive States")
        bool Immovable = false;

    /** Checks if the character has hyperarmor, can't be affected by effects that makes it lose the movement control. Controlled by timer*/
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Beat|Non Exclusive States")
        bool Hyperarmor = false;

    /** Checks if the character has hyperarmor, can't be affected by effects that makes it lose the movement control */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Beat|Non Exclusive States")
        bool StaticHyperarmor = false;
};

USTRUCT(BlueprintType)
struct FComboAccuracyMultiplierRow : public FTableRowBase
{
    GENERATED_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite)
        EBeatAccuracy Accuracy;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FFloatRange Percentages;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int32 ComboSum;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float ScoreMultiplier;
};

USTRUCT(BlueprintType)
struct FComboGaugeRow : public FTableRowBase
{
    GENERATED_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float MaxValue;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float Multiplier;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BEATPROJECT_API UStatsComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UStatsComponent();

    UPROPERTY(EditAnywhere, Category = "Beat|Stats", meta = (DisplayName = "Non Exclusive States"))
        FNonExclusiveStates NonExclusiveStates;

#pragma region Delegates
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "OnDeath Delegate"))
        FOnDeath EOnDeathDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "On Life Changed Delegate"))
        FOnLifeChange EOnLifeChangedDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "On Combo Index Up Delegate"))
        FOnComboIndexUp EOnComboIndexUpDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "On Combo Index Down Delegate"))
        FOnComboIndexDown EOnComboIndexDownDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "On Audience Increase Delegate"))
        FOnAudienceIncrease EOnAudienceIncreaseDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|Stats|EventDispatchers", meta = (DisplayName = "On Audience Decrease Delegate"))
        FOnAudienceIncrease EOnAudienceDecreaseDelegate;
#pragma endregion

    UPROPERTY(EditAnywhere, Category = "Beat|Audience", meta = (DisplayName = "Combo Accuracy DT"))
        UDataTable* ComboAccuracyDataTable;

    UPROPERTY(EditAnywhere, Category = "Beat|Audience", meta = (DisplayName = "Combo Gauge DT"))
        UDataTable* ComboGaugeDataTable;

    UPROPERTY(BlueprintReadOnly, Category = "Beat|Audience", meta = (DisplayName = "Array Row Names Combo Gauge"))
        TArray<FName> ComboSGaugeRowNames;

private:
    UPROPERTY(VisibleAnywhere, Category = "Beat|Stats", meta = (DisplayName = "Current Action"))
        EMutuallyExclusiveState CurrentExclusiveState;

#pragma region Stats
    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Stats", meta = (DisplayName = "Life", AllowPrivateAccess = "true"))
        UNumericStat* Life;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Stats", meta = (DisplayName = "Damage Taken Multiplier", AllowPrivateAccess = "true"))
        UNumericStat* DamageTakenMultiplier;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Stats", meta = (DisplayName = "Block Damage Taken Multiplier", AllowPrivateAccess = "true"))
        UNumericStat* BlockDamageTakenMultiplier;

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Stats", meta = (DisplayName = "AudienceGauge", AllowPrivateAccess = "true"))
        UNumericStat* AudienceGauge;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Beat|Stats", meta = (DisplayName = "Enable Move Input", AllowPrivateAccess = "true"))
        bool MoveInputEnable;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Beat|Stats", meta = (DisplayName = "Enable Actions Input", AllowPrivateAccess = "true"))
        bool ActionsInputEnable;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Beat|Stats", meta = (DisplayName = "HyperArmor", AllowPrivateAccess = "true"))
        bool HyperArmor;
#pragma endregion

#pragma region ComboGauge

    /* Current combo gauge level */
    UPROPERTY(BlueprintReadWrite, Category = "Beat|Stats|Combo", meta = (DisplayName = "Combo Counter", AllowPrivateAccess = "true"))
        int32 CurrentGaugeValue;

    /* Total combo without fail */
    UPROPERTY(BlueprintReadWrite, Category = "Beat|Stats|Combo", meta = (DisplayName = "Combo Streak", AllowPrivateAccess = "true"))
        int32 ComboStreak;

    UPROPERTY(BlueprintReadWrite, Category = "Beat|Stats|Combo", meta = (DisplayName = "Combo Gauge Index", AllowPrivateAccess = "true"))
        int32 CurrentComboGaugeIndex;

    /* Current Combo Gauge Max Counter For Next Level */
    int32 CurrentMaxGaugeValue;

    UPROPERTY(BlueprintReadOnly, Category = "Beat|Stats|Combo", meta = (DisplayName = "Current Combo Multiplier", AllowPrivateAccess = "true"))
        int32 CurrentComboMultiplier;

#pragma endregion

    UPROPERTY(BlueprintReadOnly, Category = "Beat|Audience", meta = (DisplayName = "Current Action Score Multiplier", AllowPrivateAccess = "true"))
        float CurrentActionScoreMultiplier;

    // Audience Decrease On Lost Beat after reaching the threshold
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Beat|Audience", meta = (DisplayName = "Audience Decrease On Lost Beat", AllowPrivateAccess = "true"))
        float AudienceDecreaseOnLostBeat;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Beat|Audience", meta = (DisplayName = "Audience Max Beat Threshold", AllowPrivateAccess = "true"))
        int32 AudienceMaxBeatThreshold;

    UPROPERTY(BlueprintReadOnly, Category = "Beat|Audience", meta = (DisplayName = "Audience Current Beat Threshold", AllowPrivateAccess = "true"))
        int32 AudienceCurrentBeatThreshold;


    UPROPERTY()
        ABeatManager* BeatManager;

public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable)
        //Set Current Action and update enabled inputs
        void SetCurrentExclusiveState(EMutuallyExclusiveState NewExclusiveState);

    UFUNCTION(BlueprintCallable)
        //Get Current Action
        EMutuallyExclusiveState GetCurrentExclusiveState();

    UFUNCTION(BlueprintCallable)
        void IncreaseNumericStat(ENumericStat Stat, float Amount, bool IsPercentage);

    UFUNCTION(BlueprintCallable)
        void DecreaseNumericStat(ENumericStat Stat, float Amount, bool IsPercentage);

    UFUNCTION(BlueprintCallable)
        void SetCurrentNumericStat(ENumericStat Stat, float Amount);

    UFUNCTION(BlueprintCallable)
        UNumericStat* GetNumericStat(ENumericStat Stat);

    UFUNCTION(BlueprintCallable)
        bool GetMoveInputEnable();

    UFUNCTION(BlueprintCallable)
        void SetMoveInputEnable(bool Value);

    UFUNCTION(BlueprintCallable)
        bool GetActionsInputEnable();

    UFUNCTION(BlueprintCallable)
        void SetActionsInputEnable(bool Value);

    UFUNCTION(BlueprintCallable)
        bool GetHyperArmor();

    UFUNCTION(BlueprintCallable)
        void SetHyperArmor(bool Value);

    UFUNCTION(BlueprintCallable)
        int GetCurrentGaugeValue();

    UFUNCTION(BlueprintCallable)
        void SumCurrentGaugeValue(float value);

    UFUNCTION(BlueprintCallable)
        void ResetCurrentGaugeValue();

    UFUNCTION(BlueprintCallable)
        int GetComboStreak();

    UFUNCTION(BlueprintCallable)
        void SumComboStreak();

    UFUNCTION(BlueprintCallable)
        void ResetComboStreak();

    UFUNCTION(BlueprintCallable)
        void IncreaseComboGauge();

    UFUNCTION(BlueprintCallable)
        void DecreaseComboGauge();

    UFUNCTION(BlueprintCallable)
        void SetCurrentActionScoreMul(float NewMultiplier);

    UFUNCTION()
        void UpdateAudienceGauge(UBeatAction* Action, EBeatAccuracy ActionAccuracy);

    UFUNCTION()
        void OnLostBeat();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;
};
