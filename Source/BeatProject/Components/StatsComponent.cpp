#include "StatsComponent.h"
#include "AIController.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "BeatProject/Core/NumericStat.h"
#include "BeatProject/Managers/BeatManager.h"
#include "BeatProject/Actions/BeatAction.h"
#include "Kismet/GameplayStatics.h"


// Sets default values for this component's properties
UStatsComponent::UStatsComponent()
    : CurrentExclusiveState(EMutuallyExclusiveState::IdleRun)
    , MoveInputEnable(true)
    , ActionsInputEnable(true)
    , HyperArmor(false)
    , CurrentGaugeValue(0)
    , ComboStreak(0)
    , CurrentComboGaugeIndex(1)
    , CurrentActionScoreMultiplier(0.f)
    , AudienceDecreaseOnLostBeat(100.f)
    , AudienceMaxBeatThreshold(3)
    , AudienceCurrentBeatThreshold(0)
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UStatsComponent::BeginPlay()
{
    Super::BeginPlay();

    if (ComboAccuracyDataTable->IsValidLowLevel())
    {
        ComboSGaugeRowNames = ComboAccuracyDataTable->GetRowNames();

        CurrentMaxGaugeValue = ComboGaugeDataTable->FindRow<FComboGaugeRow>(*FString::FromInt(CurrentComboGaugeIndex), TEXT(""))->MaxValue;
    }

    BeatManager = Cast<ABeatManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABeatManager::StaticClass()));
    //ABeatProjectCharacter* Owner = Cast<ABeatProjectCharacter>(GetOwner());
    if (UGameplayStatics::GetPlayerCharacter(GetWorld(), 0) == GetOwner())
    {
        BeatManager->OnLostBeatDelegate.AddDynamic(this, &UStatsComponent::DecreaseComboGauge);
        BeatManager->OnPlayAction.AddDynamic(this, &UStatsComponent::UpdateAudienceGauge);
        BeatManager->OnLostBeatDelegate.AddDynamic(this, &UStatsComponent::OnLostBeat);
    }
}

// Called every frame
void UStatsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


}

void UStatsComponent::IncreaseNumericStat(ENumericStat Stat, float Amount, bool IsPercentage)
{
    switch (Stat)
    {
    case ENumericStat::Life:
    {
        if (IsPercentage)
        {
            Life->ModifyCurrentPercentage(Amount);
        }
        else
        {
            Life->ModifyCurrentScalar(Amount);
        }

        EOnLifeChangedDelegate.Broadcast(Life->GetCurrent());
    }
    break;
    case ENumericStat::DamageTakenMult:
    {
        if (IsPercentage)
        {
            DamageTakenMultiplier->ModifyCurrentPercentage(Amount);
        }
        else
        {
            DamageTakenMultiplier->ModifyCurrentScalar(Amount);
        }
    }
    break;
    case ENumericStat::BlockDamageTakenMult:
    {
        if (IsPercentage)
        {
            BlockDamageTakenMultiplier->ModifyCurrentPercentage(Amount);
        }
        else
        {
            BlockDamageTakenMultiplier->ModifyCurrentScalar(Amount);
        }
    }
    break;
    case ENumericStat::AudienceGauge:
    {
        if (IsPercentage)
        {
            AudienceGauge->ModifyCurrentPercentage(Amount);
        }
        else
        {
            AudienceGauge->ModifyCurrentScalar(Amount);
        }
    }
    break;
    default:;
    }
}

void UStatsComponent::DecreaseNumericStat(ENumericStat Stat, float Amount, bool IsPercentage)
{
    switch (Stat)
    {
    case ENumericStat::Life:
    {
        if (CurrentExclusiveState == EMutuallyExclusiveState::Block)
        {
            if (IsPercentage)
            {
                Life->ModifyCurrentPercentage(-Amount * BlockDamageTakenMultiplier->GetCurrent());
            }
            else
            {
                Life->ModifyCurrentScalar(-Amount * BlockDamageTakenMultiplier->GetCurrent());
            }
        }
        else
        {
            if (IsPercentage)
            {
                Life->ModifyCurrentPercentage(-Amount * DamageTakenMultiplier->GetCurrent());
            }
            else
            {
                Life->ModifyCurrentScalar(-Amount * DamageTakenMultiplier->GetCurrent());
            }
        }

        if (Life->GetCurrent() <= Life->GetMin())
        {
            EOnDeathDelegate.Broadcast();
        }

        EOnLifeChangedDelegate.Broadcast(Life->GetCurrent());
    }
    break;
    case ENumericStat::DamageTakenMult:
    {
        if (IsPercentage)
        {
            DamageTakenMultiplier->ModifyCurrentPercentage(-Amount);
        }
        else
        {
            DamageTakenMultiplier->ModifyCurrentScalar(-Amount);
        }
    }
    break;
    case ENumericStat::BlockDamageTakenMult:
    {
        if (IsPercentage)
        {
            BlockDamageTakenMultiplier->ModifyCurrentPercentage(-Amount);
        }
        else
        {
            BlockDamageTakenMultiplier->ModifyCurrentScalar(-Amount);
        }
    }
    break;
    case ENumericStat::AudienceGauge:
    {
        if (IsPercentage)
        {
            AudienceGauge->ModifyCurrentPercentage(-Amount);
        }
        else
        {
            AudienceGauge->ModifyCurrentScalar(-Amount);
        }
    }
    break;
    }
}

void UStatsComponent::SetCurrentNumericStat(ENumericStat Stat, float Amount)
{
    switch (Stat)
    {
    case ENumericStat::Life:
    {
        Life->SetCurrent(Amount);

        if (Life->GetCurrent() <= Life->GetMin())
        {
            EOnDeathDelegate.Broadcast();
        }

        EOnLifeChangedDelegate.Broadcast(Life->GetCurrent());
    }
    break;
    case ENumericStat::DamageTakenMult:
    {
        DamageTakenMultiplier->SetCurrent(Amount);
    }
    break;
    case ENumericStat::BlockDamageTakenMult:
    {
        BlockDamageTakenMultiplier->SetCurrent(Amount);
    }
    break;
    }
}

UNumericStat* UStatsComponent::GetNumericStat(ENumericStat Stat)
{
    switch (Stat)
    {
    case ENumericStat::Life:
    {
        return Life;
    }
    break;
    case ENumericStat::DamageTakenMult:
    {
        return DamageTakenMultiplier;
    }
    break;
    case ENumericStat::BlockDamageTakenMult:
    {
        return BlockDamageTakenMultiplier;
    }
    break;
    }

    return nullptr;
}

bool UStatsComponent::GetMoveInputEnable()
{
    return MoveInputEnable;
}

void UStatsComponent::SetMoveInputEnable(bool Value)
{
    MoveInputEnable = Value;
}

bool UStatsComponent::GetActionsInputEnable()
{
    return ActionsInputEnable;
}

void UStatsComponent::SetActionsInputEnable(bool Value)
{
    ActionsInputEnable = Value;
}

bool UStatsComponent::GetHyperArmor()
{
    return HyperArmor;
}

void UStatsComponent::SetHyperArmor(bool Value)
{
    HyperArmor = Value;
}

int UStatsComponent::GetCurrentGaugeValue()
{
    return CurrentGaugeValue;
}

void UStatsComponent::SumCurrentGaugeValue(float value)
{
    CurrentGaugeValue += value;
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Gauge %d"), CurrentGaugeValue));
    // Index Up
    if (CurrentGaugeValue == CurrentMaxGaugeValue)
    {
        IncreaseComboGauge();
    }
}

void UStatsComponent::ResetCurrentGaugeValue()
{
    CurrentGaugeValue = 0;
}

int UStatsComponent::GetComboStreak()
{
    return ComboStreak;
}

void UStatsComponent::SumComboStreak()
{
    ComboStreak++;
}

void UStatsComponent::ResetComboStreak()
{
    ComboStreak = 0;
}

void UStatsComponent::IncreaseComboGauge()
{
    CurrentComboGaugeIndex++;
    int TopIndex = ComboGaugeDataTable->GetRowNames().Num();
    CurrentComboGaugeIndex = FMath::Clamp(CurrentComboGaugeIndex, 1, TopIndex);

    // Get next index max Gauge value
    CurrentMaxGaugeValue = ComboGaugeDataTable->FindRow<FComboGaugeRow>(*FString::FromInt(CurrentComboGaugeIndex), TEXT(""))->MaxValue;

    CurrentGaugeValue = 0;

    CurrentComboMultiplier = ComboGaugeDataTable->FindRow<FComboGaugeRow>(*FString::FromInt(CurrentComboGaugeIndex), TEXT(""))->Multiplier;
    EOnComboIndexUpDelegate.Broadcast(CurrentComboMultiplier);
}

void UStatsComponent::DecreaseComboGauge()
{
    if (CurrentGaugeValue == 0)
    {
        CurrentComboGaugeIndex--;

        CurrentComboGaugeIndex = FMath::Max(CurrentComboGaugeIndex, 1);

        // Get next index max Gauge value
        CurrentMaxGaugeValue = ComboGaugeDataTable->FindRow<FComboGaugeRow>(*FString::FromInt(CurrentComboGaugeIndex), TEXT(""))->MaxValue;

        CurrentGaugeValue = 0;

        CurrentComboMultiplier = ComboGaugeDataTable->FindRow<FComboGaugeRow>(*FString::FromInt(CurrentComboGaugeIndex), TEXT(""))->Multiplier;
        EOnComboIndexDownDelegate.Broadcast(CurrentComboMultiplier);
    }
    else
    {
        ResetCurrentGaugeValue();
    }

    ResetComboStreak();
}

void UStatsComponent::SetCurrentActionScoreMul(float NewMultiplier)
{
    CurrentActionScoreMultiplier = NewMultiplier;
}

void UStatsComponent::UpdateAudienceGauge(UBeatAction* Action, EBeatAccuracy ActionAccuracy)
{
    if (Action->IsValidLowLevel() && AudienceGauge->IsValidLowLevel())
    {
        if (ActionAccuracy != EBeatAccuracy::Fail && ActionAccuracy != EBeatAccuracy::Poor)
        {
            const float TotalAudienceIncrease = CurrentActionScoreMultiplier * Action->ActionValue * CurrentComboMultiplier;

            IncreaseNumericStat(ENumericStat::AudienceGauge, TotalAudienceIncrease, false);


            EOnAudienceIncreaseDelegate.Broadcast(Action, TotalAudienceIncrease);
        }
        else
        {
            const float DecreaseValue = Action->LowAccuracyActionValue;
            DecreaseNumericStat(ENumericStat::AudienceGauge, DecreaseValue, false);

            EOnAudienceDecreaseDelegate.Broadcast(Action, DecreaseValue);
        }

        AudienceCurrentBeatThreshold = 0;
    }
}

void UStatsComponent::OnLostBeat()
{
    AudienceCurrentBeatThreshold++;
    AudienceCurrentBeatThreshold = FMath::Clamp(AudienceCurrentBeatThreshold, 0, AudienceMaxBeatThreshold);

    if (AudienceCurrentBeatThreshold == AudienceMaxBeatThreshold)
    {
        DecreaseNumericStat(ENumericStat::AudienceGauge, AudienceDecreaseOnLostBeat, false);
    }
}

void UStatsComponent::SetCurrentExclusiveState(EMutuallyExclusiveState NewAction)
{
    CurrentExclusiveState = NewAction;

    switch (NewAction)
    {
    case EMutuallyExclusiveState::IdleRun:
    {
        MoveInputEnable = true;
        ActionsInputEnable = true;
    }
    break;
    case EMutuallyExclusiveState::Dash:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    case EMutuallyExclusiveState::Attack:
    {
        MoveInputEnable = false;
        ActionsInputEnable = true;
    }
    break;
    case EMutuallyExclusiveState::Block:
    {
        MoveInputEnable = true;
        ActionsInputEnable = true;
    }
    break;
    case EMutuallyExclusiveState::Parry:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    case EMutuallyExclusiveState::Stun:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    case EMutuallyExclusiveState::Hit:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    case EMutuallyExclusiveState::Dead:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    case EMutuallyExclusiveState::Parried:
    {
        MoveInputEnable = false;
        ActionsInputEnable = false;
    }
    break;
    default:;
    }
}

EMutuallyExclusiveState UStatsComponent::GetCurrentExclusiveState()
{
    return CurrentExclusiveState;
}
