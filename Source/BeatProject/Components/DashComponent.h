// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityComponent.h"
#include "Components/TimelineComponent.h"

#include "DashComponent.generated.h"

#pragma region Forward declarations
class UDashAction;
#pragma endregion 

UCLASS()
class BEATPROJECT_API UDashComponent : public UAbilityComponent
{
    GENERATED_BODY()

public:
    UDashComponent();

    UPROPERTY(EditAnywhere, Category = "Beat|Dash|Land", meta = (DisplayName = "Land Dash Action"))
        TSubclassOf<UDashAction> LandDashActionBP;
    UPROPERTY()
        UDashAction* LandDashAction;

    UPROPERTY(EditAnywhere, Category = "Beat|Dash|Air", meta = (DisplayName = "Air Dash Action"))
        TSubclassOf<UDashAction> AirDashActionBP;
    UPROPERTY()
        UDashAction* AirDashAction;

protected:
    virtual void BeginPlay() override;
    virtual bool CheckPreconditions() override;
    virtual void Cancel(UBaseAction* InstigatorAction) override;

    bool ExecuteAbility() override;

private:
    void FinishDash();

};
