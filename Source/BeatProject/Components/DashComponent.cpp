// Fill out your copyright notice in the Description page of Project Settings.


#include "DashComponent.h"
#include "BeatProject/BeatProjectCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BeatProject/Managers/BeatManager.h"
#include "BeatProject/Actions/DashAction.h"
#include "Engine.h"

UDashComponent::UDashComponent()
{
}

void UDashComponent::BeginPlay()
{
    Super::BeginPlay();

    OwnerCharacter = Cast<ABeatProjectCharacter>(GetOwner());
    BeatManager = Cast<ABeatManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABeatManager::StaticClass()));

    if (LandDashActionBP->IsValidLowLevel())
    {
        LandDashAction = NewObject<UDashAction>(this, LandDashActionBP, "LandDash", RF_NoFlags, LandDashActionBP->GetDefaultObject(), true);
        LandDashAction->InitializeAction(*OwnerCharacter, *BeatManager);
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "LandDashAction is missing in " + GetName());
        FMessageLog("PIE").Error(FText::FromString(TEXT("LandDashAction is missing in" + GetName())));
    }

    if (AirDashActionBP->IsValidLowLevel())
    {
        AirDashAction = NewObject<UDashAction>(this, AirDashActionBP, "AirDash", RF_NoFlags, AirDashActionBP->GetDefaultObject(), true);
        AirDashAction->InitializeAction(*OwnerCharacter, *BeatManager);
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "AirDashAction is missing in " + GetName());
        FMessageLog("PIE").Error(FText::FromString(TEXT("AirDashAction is missing in" + GetName())));
    }
}

bool UDashComponent::CheckPreconditions()
{
    if (Super::CheckPreconditions() && LandDashAction->GetActionState() == EActionState::Available && AirDashAction->GetActionState() == EActionState::Available)
    {
        return true;
    }

    return false;
}

void UDashComponent::Cancel(UBaseAction* InstigatorAction)
{
    if (LandDashAction->GetActionState() == EActionState::Executing)
    {
        LandDashAction->Cancel(InstigatorAction);
    }
    else if(AirDashAction->GetActionState() == EActionState::Executing)
    {
        AirDashAction->Cancel(InstigatorAction);
    }
}

bool UDashComponent::ExecuteAbility()
{
    if (OwnerCharacter->GetMovementComponent()->IsFalling())
    {
         return AirDashAction->TryPlayAction();
    }
    else
    {
         return LandDashAction->TryPlayAction();
    }
}
