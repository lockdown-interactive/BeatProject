﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "BeatProjectCharacter.h"

#include "AIController.h"
#include "BrainComponent.h"
#include "Actions/BeatAction.h"
#include "Actions/JumpAction.h"
#include "Actions/ParriedAction.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "BeatProject/Components/DashComponent.h"
#include "BeatProject/Components/CombatComponent.h"
#include "Components/StatsComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Managers/BeatManager.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "BeatProject/Actions/DeadAction.h"
#include "BeatProject/Actions/StunAction.h"
#include "BeatProject/Actions/ParryAction.h"
#include "BehaviorTree/BlackboardComponent.h"


//////////////////////////////////////////////////////////////////////////
// ABeatProjectCharacter

ABeatProjectCharacter::ABeatProjectCharacter()
{
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

    // set our turn rates for input
    BaseTurnRate = 45.f;
    BaseLookUpRate = 45.f;

    // Don't rotate when the controller rotates. Let that just affect the camera.
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
    GetCharacterMovement()->JumpZVelocity = 600.f;
    GetCharacterMovement()->AirControl = 0.2f;

    // Create a camera boom (pulls in towards the player if there is a collision)
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
    CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

    // Create a follow camera
    FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
    FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
    FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

//Create Components
    StatsComponent = CreateDefaultSubobject<UStatsComponent>(TEXT("CharacterStatsComponent"));
    CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("CombatComponent"));
    DashComponent = CreateDefaultSubobject<UDashComponent>(TEXT("DashComponent"));

    MovementCapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("MovementCapsuleComponent"));
    MovementCapsuleComponent->InitCapsuleSize(34.0f, 70.0f);
    MovementCapsuleComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
    MovementCapsuleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    MovementCapsuleComponent->CanCharacterStepUpOn = ECB_No;
    MovementCapsuleComponent->SetupAttachment(GetCapsuleComponent());

    OverlapDisplacementVelocity = 250.f;

}

//////////////////////////////////////////////////////////////////////////
// Input

void ABeatProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    // Set up gameplay key bindings
    check(PlayerInputComponent);
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABeatProjectCharacter::TryPlayJump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
    PlayerInputComponent->BindAction("Attack", IE_Pressed, CombatComponent, &UCombatComponent::AttackOnBeat);
    PlayerInputComponent->BindAction("Parry", IE_Pressed, CombatComponent, &UCombatComponent::BlockOnBeat);
    PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &ABeatProjectCharacter::TryPlayDash);

    PlayerInputComponent->BindAxis("MoveForward", this, &ABeatProjectCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ABeatProjectCharacter::MoveRight);

    // We have 2 versions of the rotation bindings to handle different kinds of devices differently
    // "turn" handles devices that provide an absolute delta, such as a mouse.
    // "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
    PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis("TurnRate", this, &ABeatProjectCharacter::TurnAtRate);
    PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("LookUpRate", this, &ABeatProjectCharacter::LookUpAtRate);

}

void ABeatProjectCharacter::TryPlayJump()
{
    JumpAction->TryPlayAction();
}

void ABeatProjectCharacter::TryPlayDash()
{
    DashComponent->TryExecuteAbility();
}

void ABeatProjectCharacter::BeginPlay()
{
    Super::BeginPlay();

    BeatManager = Cast<ABeatManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABeatManager::StaticClass()));

    //Initialize Actions
    if (JumpAction->IsValidLowLevel())
    {
        JumpAction->InitializeAction(*this, *BeatManager);
    }
    if (DeadAction->IsValidLowLevel())
    {
        DeadAction->InitializeAction(*this, *BeatManager);
    }
    if (ParryAction->IsValidLowLevel())
    {
        ParryAction->InitializeAction(*this, *BeatManager);
    }
    if (ParriedAction->IsValidLowLevel())
    {
        ParriedAction->InitializeAction(*this, *BeatManager);
    }
    if (StunAction->IsValidLowLevel())
    {
        StunAction->InitializeAction(*this, *BeatManager);
    }

    for (auto& Tuple : HitActions)
    {
        for (int Index = 0; Index < Tuple.Value.ArrayActions.Num(); ++Index)
        {
            
            UBaseAction* HitAction = Tuple.Value.ArrayActions[Index];
            HitAction->InitializeAction(*this, *BeatManager);
        }
    }

    for (UBaseAction* HitAction : BlockHitActions.ArrayActions)
    {
        if (HitAction->IsValidLowLevel())
        {
            HitAction->InitializeAction(*this, *BeatManager);
        }
    }

    if(StunAction->IsValidLowLevel())
    {
        StunAction->InitializeAction(*this, *BeatManager);
    }

    DefaultGravityScale = GetCharacterMovement()->GravityScale;
    DefaultFrictionFactor = GetCharacterMovement()->BrakingFrictionFactor;

    //Bind functions to events
    EOnHitDelegate.AddDynamic(this, &ABeatProjectCharacter::OnEventHit);
    EOnHitReceivedDelegate.AddDynamic(this, &ABeatProjectCharacter::OnEventHitReceived);
    StatsComponent->EOnDeathDelegate.AddDynamic(this, &ABeatProjectCharacter::OnEventDeath);
    
    ParriedAction->OnActionEndDelegate.AddDynamic(this, &ABeatProjectCharacter::ParriedToStun);
}

void ABeatProjectCharacter::ParriedToStun()
{
    StunAction->TryPlayAction();
}

void ABeatProjectCharacter::OnEventHit(ABeatProjectCharacter* HitReceivedCharacter, EAttackType AttackType, float Damage)
{
}

void ABeatProjectCharacter::OnEventHitReceived(ABeatProjectCharacter* HitInstigatorCharacter, EAttackType AttackType, float Damage,
    bool IsPercent)
{
    StatsComponent->DecreaseNumericStat(ENumericStat::Life, Damage, IsPercent);

    if (StatsComponent->GetCurrentExclusiveState() == EMutuallyExclusiveState::Block) //Hit received while blocking
    {
        int Rand = FMath::RandRange(0, BlockHitActions.ArrayActions.Num() - 1);
        if (BlockHitActions.ArrayActions.Num() > 0 && BlockHitActions.ArrayActions[Rand]->IsValidLowLevel())
        {
            BlockHitActions.ArrayActions[Rand]->TryPlayAction();
        }
    }
    else
    {
        int Rand = FMath::RandRange(0, HitActions[AttackType].ArrayActions.Num() - 1);
        if (HitActions[AttackType].ArrayActions[Rand]->IsValidLowLevel())
        {
            HitActions[AttackType].ArrayActions[Rand]->TryPlayAction();
        }
    }
}

void ABeatProjectCharacter::OnEventDeath()
{
    AAIController* DeathCharacterController = Cast<AAIController>(GetController());
    if (DeathCharacterController)
    {
        DeathCharacterController->BrainComponent->StopLogic("Because Death");
    }

    DeadAction->TryPlayAction();
}

UBeatAction* ABeatProjectCharacter::GetCurrentBeforeOffsetAction()
{
    return CurrentBeforeOffsetAction;
}

void ABeatProjectCharacter::SetCurrentBeforeOffsetAction(UBeatAction* _CurrentBeforeOffsetAction)
{
    if(_CurrentBeforeOffsetAction->IsValidLowLevel())
    {
        CurrentBeforeOffsetAction = _CurrentBeforeOffsetAction;
        BeatManager->OnBeatDelegate.AddDynamic(CurrentBeforeOffsetAction, &UBeatAction::OnTimelineBeat);
    }
}

UCapsuleComponent* ABeatProjectCharacter::GetMovementCapsuleComponent()
{
    return MovementCapsuleComponent;
}

void ABeatProjectCharacter::TurnAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABeatProjectCharacter::LookUpAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ABeatProjectCharacter::MoveForward(float Value)
{
    if(StatsComponent->GetMoveInputEnable())
    {
        if ((Controller != NULL) && (Value != 0.0f))
        {
            // find out which way is forward
            const FRotator Rotation = Controller->GetControlRotation();
            const FRotator YawRotation(0, Rotation.Yaw, 0);

            // get forward vector
            const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

            // add movement in that direction
            AddMovementInput(Direction, Value);
        }
    }
}

void ABeatProjectCharacter::MoveRight(float Value)
{
    if (StatsComponent->GetMoveInputEnable())
    {
        if ((Controller != NULL) && (Value != 0.0f))
        {
            // find out which way is right
            const FRotator Rotation = Controller->GetControlRotation();
            const FRotator YawRotation(0, Rotation.Yaw, 0);

            // get right vector 
            const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

            // add movement in that direction
            AddMovementInput(Direction, Value);
        }
    }
}

FVector ABeatProjectCharacter::GetPlayerLastInput()
{
    //Player input
    float x = InputComponent->GetAxisValue("MoveForward");
    float y = InputComponent->GetAxisValue("MoveRight");

    FVector cameraForward = GetFollowCamera()->GetForwardVector().GetSafeNormal();
    FVector cameraRight = GetFollowCamera()->GetRightVector().GetSafeNormal();
    //Get the input direction vector according to the camera rotation 
    FVector vInput = FVector(x * cameraForward + cameraRight * y);
    vInput.Z = 0.f;
    vInput = vInput.GetSafeNormal();
    return vInput;
}

FVector ABeatProjectCharacter::GetPlayerLastInputOrForward()
{
    FVector Input = GetPlayerLastInput();
    if (Input == FVector::ZeroVector)
    {
        Input = GetActorForwardVector();
    }
    return Input;
}

void ABeatProjectCharacter::SetCharacterRotationToInput()
{
    FRotator oldRotation = GetActorRotation();
    FVector playerPos = GetActorLocation();
    FVector Input = GetPlayerLastInput();
    if (Input == FVector::ZeroVector)
    {
        Input = GetActorForwardVector();
    }

    Input *= FVector(1000.f, 1000.f, 0.f);
    Input += playerPos;
    FRotator newRotation = oldRotation;
    newRotation.Yaw = UKismetMathLibrary::FindLookAtRotation(playerPos, Input).Yaw;
    SetActorRotation(newRotation);
}