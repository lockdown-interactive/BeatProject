// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BeatProject/BeatProject.h"
#include "BeatProjectCharacter.generated.h"

#pragma region ForwardDeclaration
class UDataTable;
class UJumpAction;
class UParriedAction;
class UParryAction;
class UStunAction;
class UDeadAction;
class USpringArmComponent;
class UCameraComponent;
class UDashComponent;
class UCombatComponent;
class UStatsComponent;
class ABeatManager;
class UBeatAction;
class UBaseAction;
class UDatatable;
#pragma endregion

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHit, class ABeatProjectCharacter*, HitReceivedCharacter, EAttackType, AttackType, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnParry, class ABeatProjectCharacter*, ParryReceivedCharacter);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnHitReceived, class ABeatProjectCharacter*, HitInstigatorCharacter, EAttackType, AttackType, float, Damage, bool, IsPercent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnParryReceived, class ABeatProjectCharacter*, ParryInstigator);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackInterrupted);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDashStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDashEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDashInterrupted);


USTRUCT(BlueprintType)
struct FActionsArray
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Beat|Actions")
        TArray<UBaseAction*> ArrayActions;
};

UCLASS(config = Game)
class ABeatProjectCharacter : public ACharacter
{
    GENERATED_BODY()

#pragma region Components
    /** Camera boom positioning the camera behind the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Camera", meta = (AllowPrivateAccess = "true"))
        USpringArmComponent* CameraBoom;

    /** Follow camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Camera", meta = (AllowPrivateAccess = "true"))
        UCameraComponent* FollowCamera;

    /** Dash Component */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Dash", meta = (AllowPrivateAccess = "true"))
        UDashComponent* DashComponent;

    /** Dash Capsule Component */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Dash", meta = (AllowPrivateAccess = "true"))
        UCapsuleComponent* MovementCapsuleComponent;
#pragma  endregion 

public:
    ABeatProjectCharacter();

#pragma region Components

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Stats", meta = (DisplayName = "Stats Component", AllowPrivateAccess = "true"))
        UStatsComponent* StatsComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Combat", meta = (DisplayName = "Combat Component", AllowPrivateAccess = "true"))
        UCombatComponent* CombatComponent;
#pragma endregion

#pragma region Actions
    UPROPERTY(Instanced, EditAnyWhere, BlueprintReadWrite, Category = "Beat|NonExclusiveActions", meta = (DisplayName = "Jump"))
        UJumpAction* JumpAction;

    UPROPERTY(Instanced, EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Dead"))
        UDeadAction* DeadAction;

    //Hit Actions to play depending on the Attack received
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Hits"))
        TMap<EAttackType, FActionsArray> HitActions;

    //Hit Actions to play while the character is Blocking
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Block Hits"))
        FActionsArray BlockHitActions;

    UPROPERTY(Instanced, EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Stun"))
        UStunAction* StunAction;

    UPROPERTY(Instanced, EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Parry"))
        UParryAction* ParryAction;

    UPROPERTY(Instanced, EditAnyWhere, BlueprintReadWrite, Category = "Beat|ExclusiveActions", meta = (DisplayName = "Parried"))
        UParriedAction* ParriedAction;
#pragma endregion 

#pragma region Camera
    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Camera")
        float BaseTurnRate;

    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Camera")
        float BaseLookUpRate;
#pragma endregion

#pragma region Managers
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|Managers", meta = (DisplayName = "Beat Manager"))
        ABeatManager* BeatManager;
#pragma endregion 

    /* Overlap Displacement Velocity in Unreals per second (Always must be greater than 0) */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Beat|Displacement", meta = (DisplayName = "Overlap Displacement Velocity", ClampMin = "50"))
        float OverlapDisplacementVelocity;

    /** Current Active Action */
    UPROPERTY(BlueprintReadOnly, Category = "Beat|Action", meta = (DisplayName = "Current Active Action"))
        UBaseAction* CurrentAction;

    /** Current Before Offset Action */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Beat|OffsetAction", meta = (AllowPrivateAccess = "true"))
        UBeatAction* CurrentBeforeOffsetAction;

    //Store character movement component initial values
    float DefaultGravityScale;
    float DefaultFrictionFactor;

#pragma region EventDispatchers
    //When I hit another Game Character
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Hit Delegate"))
        FOnHit EOnHitDelegate;

    //When I get Hit by another Game Character
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Hit Received Delegate"))
        FOnHitReceived EOnHitReceivedDelegate;

    //When a character has executed a parry
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Parry Delegate"))
        FOnParry EOnParryDelegate;

    //When a character has received a parry
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Parry Received Delegate"))
        FOnParryReceived EOnParryReceivedDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Attack Start Delegate"))
        FOnAttackStart OnAttackStartDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Attack End Delegate"))
        FOnAttackEnd OnAttackEndDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Attack Interrupted Delegate"))
        FOnAttackInterrupted OnAttackInterruptedDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Dash Start Delegate"))
        FOnDashStart OnDashStartDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Dash End Delegate"))
        FOnDashEnd OnDashEndDelegate;

    UPROPERTY(BlueprintAssignable, Category = "Beat|EventDispatchers", meta = (DisplayName = "On Dash Interrupt Delegate"))
        FOnDashInterrupted OnDashInterruptDelegate;
#pragma endregion


protected:

    /** Called for forwards/backward input */
    void MoveForward(float Value);

    /** Called for side to side input */
    void MoveRight(float Value);

    /**
     * Called via input to turn at a given rate.
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void TurnAtRate(float Rate);

    /**
     * Called via input to turn look up/down at a given rate.
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void LookUpAtRate(float Rate);

protected:
    // APawn interface
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    // End of APawn interface

    virtual void BeginPlay() override;

    UFUNCTION()
        void OnEventHit(class ABeatProjectCharacter* HitReceivedCharacter, EAttackType AttackType, float Damage);

    UFUNCTION()
        void OnEventHitReceived(class ABeatProjectCharacter* HitInstigatorCharacter, EAttackType AttackType, float Damage, bool IsPercent);

    UFUNCTION()
        void OnEventDeath();

    UFUNCTION()
        void ParriedToStun();

    UFUNCTION()
        void TryPlayJump();

    UFUNCTION()
        void TryPlayDash();

public:
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
    /** Returns FollowCamera subobject **/
    FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

    UFUNCTION()
        UBeatAction* GetCurrentBeforeOffsetAction();

    UFUNCTION()
        void SetCurrentBeforeOffsetAction(UBeatAction* _CurrentBeforeOffsetAction);

    UFUNCTION()
        UCapsuleComponent* GetMovementCapsuleComponent();

    UFUNCTION()
        FVector GetPlayerLastInput();

    UFUNCTION()
        FVector GetPlayerLastInputOrForward();

    UFUNCTION()
        void SetCharacterRotationToInput();
};

